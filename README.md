LICENSE
=======

 Imagepipe

 Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 Imagepipe is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.

CREDITS
=======

 The Material Design icons are Copyright (c) Google Inc., licensed 
 under the Apache License Version 2.0.
 
 This app uses gradle and the gradle wrapper, Copyright (c) Gradle Inc.,
 licensed under the Apache 2.0 license.

Contributors:
-------------

- mondstern (french and dutch translation)
- mezysinc (portugese translation)
 
GETTING THE BINARY
==================

From the main F-Droid repository (recommended)
----------------------------------------------

Imagepipe is available on the F-Droid main repository.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" 
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/de.kaffeemitkoffein.imagepipe)

From the author's custom F-Droid repository.
--------------------------------------------

 Alternatively, the app is also available in the author's custom repository. 

 You need to add the following repo to your
 f-droid app:
 
 Repo: https://kaffeemitkoffein.de
 
 Fingerprint: 2F 27 5C B8 37 35 FE 79 75 44 9C E8 00 CA 84 07 B5 01 9D 5F 7B 6A BC B3 0F 36 51 C5 20 A3 72 61
 
### Here is a short instruction how to add this repo:
 
 1. Launch the f-droid app
 2. Select "Settings"
 3. Select "Repositories" from the list
 4. Select "+NEW REPOSITORY"
 5. Add the following repository address: "https://kaffeemitkoffein.de"
 6. Optional: enter the fingerprint of this repo (see above)
 7. Select "Add"
 
### Now, you can install this app via f-droid as soon as the app updated the repo data:
 
 1. In the main view, update the f-droid repos by swiping down
 2. Look for the Imagepipe app in f-droid and install it
 
Direct download of the binary (APK)
-----------------------------------
 
 You can also directly download the apk file via this link:
 
 <https://kaffeemitkoffein.de/fdroid/repo/Imagepipe.apk>
 


CONTRIBUTING
============

 The source code of Imagepipe is available on codeberg.org:
 
 <https://codeberg.org/Starfish/Imagepipe>

 The source code can also be downloaded here:
 https://kaffeemitkoffein.de/source/Imagepipe_source.tar.gz

 For suggestions and bug reports, please contact the author:
 pawel (at) kaffeemitkoffein.de


