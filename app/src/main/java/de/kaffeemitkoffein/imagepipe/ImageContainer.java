/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.net.Uri;
import java.io.File;

/**
 * This is a tiny class to combine an image uri and a file for easier handling in lists and arrays.
 * There is no easy and reliable way to convert a file to an uri and vice versa.
 *
 * Rule: each child of this class always holds an URI and sometimes also the file and a filesize.
 */

public class ImageContainer {
    public Uri uri;
    public File file;
    public long filesize=0;

    public ImageContainer(Uri u, File f, long filesize){
        this.uri = u;
        this.file = f;
        this.filesize = filesize;
    }

    public ImageContainer(File f){
        this.file = f;
    }

    public ImageContainer(Uri u){
        this.uri = u;
    }

    public ImageContainer(Uri u, long filesize){
        this.uri = u;
        this.filesize = filesize;
    }

    public ImageContainer(ImageContainer imageContainer){
        if (imageContainer.uri!=null){
            this.uri = imageContainer.uri;
        }
        if (imageContainer.file!=null){
            this.file = imageContainer.file;
        }
        this.filesize = imageContainer.filesize;
    }

    public ImageContainer(){
    }
}
