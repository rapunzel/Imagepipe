package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;

public class ImagepipePreferences {

    public static final String PREF_SCALEMAX = "PREF_scale_max_diameter";
    private static final String PREF_FILENAME = "PREF_filename";
    private static final String PREF_QUALITY = "PREF_quality";
    private static final String PREF_MAXQUALITY = "PREF_maxquality";
    public static final String PREF_COMPRESSFORMAT = "PREF_compressformat";
    private static final String PREF_AUTOPIPE = "PREF_autopipe";
    private static final String PREF_NUMBERING = "PREF_numbering";
    private static final String PREF_AUTOSCALE = "PREF_autoscale";
    private static final String PREF_AUTOROTATE = "PREF_autorotate";
    private static final String PREF_PREVIEWQUALITY = "PREF_previewquality";
    private static final String PREF_FORCEDOWNSCALE = "PREF_forcedownscale";
    private static final String PREF_SMARTCROP="PREF_smartcrop";
    private static final String PREF_BUBLLECOUNTER = "PREF_bubblecounter";
    private static final String PREF_SHOWHINTS = "PREF_showhints";
    private static final String PREF_IC_URI = "PREF_ic_uri";
    private static final String PREF_IC_FILESIZE = "PREF_ic_filesize";

    private static final String PREF_SCALEMAX_DEFAULT = "1024";
    private static final String PREF_FILENAME_DEFAULT = "Imagepipe";
    private static final int PREF_QUALITY_DEFAULT = 70;
    private static final String PREF_MAXQUALITY_DEFAULT = "80";
    private static final String PREF_COMPRESSFORMAT_DEFAULT = ImageFileFormat.JPEG;
    private static final boolean PREF_AUTOPIPE_DEFAULT = true;
    private static final String PREF_NUMBERING_DEFAULT = "1";
    private static final boolean PREF_AUTOSCALE_DEFAULT = true;
    private static final boolean PREF_AUTOROTATE_DEFAULT = true;
    private static final boolean PREF_PREVIEWQUALITY_DEFAULT = true;
    private static final boolean PREF_FORCEDOWNSCALE_DEFAULT = true;
    private static final boolean PREF_SMARTCROP_DEFAULT=true;
    private static final int PREF_BUBLLECOUNTER_DEFAULT = 0;
    private static final boolean PREF_SHOWHINTS_DEFAULT = true;
    private static final String PREF_IC_URI_DEFAULT = "";
    private static final long PREF_IC_FILESIZE_DEFAULT = 0;

    public String scalemax = PREF_SCALEMAX_DEFAULT;
    public String filename = PREF_FILENAME_DEFAULT;
    public int quality = PREF_QUALITY_DEFAULT;
    public String quality_maxvalue = PREF_MAXQUALITY_DEFAULT;
    public String compressformat = PREF_COMPRESSFORMAT_DEFAULT;
    public boolean autopipe = PREF_AUTOPIPE_DEFAULT;
    public String numbering = PREF_NUMBERING_DEFAULT;
    public boolean autoscale = PREF_AUTOSCALE_DEFAULT;
    public boolean autorotate = PREF_AUTOROTATE_DEFAULT;
    public boolean previewquality = PREF_PREVIEWQUALITY_DEFAULT;
    public boolean forcedownscale = PREF_FORCEDOWNSCALE_DEFAULT;
    public boolean smartcrop = PREF_SMARTCROP_DEFAULT;
    public int bubblecounter = PREF_BUBLLECOUNTER_DEFAULT;
    public boolean showHints = PREF_SHOWHINTS_DEFAULT;
    public String ic_uri = PREF_IC_URI_DEFAULT;
    public long ic_filesize = PREF_IC_FILESIZE_DEFAULT;

    private Context context;
    SharedPreferences sharedPreferences;

    public ImagepipePreferences(Context c){
        this.context = c;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        readPreferences();
    }

    public void readPreferences(){
        this.scalemax = readPreference(PREF_SCALEMAX,PREF_SCALEMAX_DEFAULT);
        this.filename = readPreference(PREF_FILENAME,PREF_FILENAME_DEFAULT);
        this.quality = readPreference(PREF_QUALITY,PREF_QUALITY_DEFAULT);
        this.quality_maxvalue = readPreference(PREF_MAXQUALITY,PREF_MAXQUALITY_DEFAULT);
        this.compressformat = readPreference(PREF_COMPRESSFORMAT,PREF_COMPRESSFORMAT_DEFAULT);
        this.autopipe = readPreference(PREF_AUTOPIPE,PREF_AUTOPIPE_DEFAULT);
        this.numbering = readPreference(PREF_NUMBERING,PREF_NUMBERING_DEFAULT);
        this.autoscale = readPreference(PREF_AUTOSCALE,PREF_AUTOSCALE_DEFAULT);
        this.autorotate = readPreference(PREF_AUTOROTATE,PREF_AUTOROTATE_DEFAULT);
        this.previewquality = readPreference(PREF_PREVIEWQUALITY,PREF_PREVIEWQUALITY_DEFAULT);
        this.forcedownscale = readPreference(PREF_FORCEDOWNSCALE,PREF_FORCEDOWNSCALE_DEFAULT);
        this.smartcrop = readPreference(PREF_SMARTCROP,PREF_SMARTCROP_DEFAULT);
        this.bubblecounter = readPreference(PREF_BUBLLECOUNTER,PREF_BUBLLECOUNTER_DEFAULT);
        this.showHints = readPreference(PREF_SHOWHINTS,PREF_SHOWHINTS_DEFAULT);
        this.ic_uri = readPreference(PREF_IC_URI,PREF_IC_URI_DEFAULT);
        this.ic_filesize = readPreference(PREF_IC_FILESIZE,PREF_IC_FILESIZE_DEFAULT);
    }

    public void savePreferences(){
        applyPreference(PREF_SCALEMAX,this.scalemax);
        applyPreference(PREF_FILENAME,this.filename);
        applyPreference(PREF_QUALITY,this.quality);
        applyPreference(PREF_MAXQUALITY,this.quality_maxvalue);
        applyPreference(PREF_COMPRESSFORMAT,this.compressformat);
        applyPreference(PREF_AUTOPIPE,this.autopipe);
        applyPreference(PREF_NUMBERING,this.numbering);
        applyPreference(PREF_AUTOSCALE,this.autoscale);
        applyPreference(PREF_AUTOROTATE,this.autorotate);
        applyPreference(PREF_PREVIEWQUALITY,this.previewquality);
        applyPreference(PREF_FORCEDOWNSCALE,this.forcedownscale);
        applyPreference(PREF_SMARTCROP,this.smartcrop);
        applyPreference(PREF_BUBLLECOUNTER,this.bubblecounter);
        applyPreference(PREF_SHOWHINTS,this.showHints);
        applyPreference(PREF_IC_URI,this.ic_uri);
        applyPreference(PREF_IC_FILESIZE,this.ic_filesize);
    }

    public String readPreference(String p, String d){
        return sharedPreferences.getString(p,d);
    }

    public Boolean readPreference(String p, Boolean d){
        return sharedPreferences.getBoolean(p,d);
    }

    public int readPreference(String p, int d){
        return sharedPreferences.getInt(p,d);
    }

    public long readPreference(String p, long d){
        return sharedPreferences.getLong(p,d);
    }

    public void applyPreference(String pref, String value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(pref,value);
        pref_editor.apply();
    }

    public void applyPreference(String pref, Boolean value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(pref,value);
        pref_editor.apply();
    }

    public void applyPreference(String pref, int value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(pref,value);
        pref_editor.apply();
    }

    public void applyPreference(String pref, long value){
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putLong(pref,value);
        pref_editor.apply();
    }


    public int getQualitymaxvalue() {
        int i;
        try {
            i = Integer.valueOf(quality_maxvalue);
            return i;
        } catch (NumberFormatException e) {
            quality_maxvalue = PREF_MAXQUALITY_DEFAULT;
            applyPreference(PREF_MAXQUALITY,quality_maxvalue);
            return Integer.valueOf(PREF_MAXQUALITY_DEFAULT);
        }
    }

    static class ImageFileFormat {
        public final static String JPEG = "0";
        public final static String PNG = "1";
        public final static String WEBP = "2";
    }

    public static String getImageFileFormatValue(Bitmap.CompressFormat compressFormat){
        switch (compressFormat) {
            case PNG: return ImageFileFormat.PNG;
            case WEBP: return ImageFileFormat.WEBP;
        }
        return ImageFileFormat.JPEG;
    }

    public static Bitmap.CompressFormat getImageFileCompressFormat(String s){
        if (s.equals(ImageFileFormat.PNG)){
            return Bitmap.CompressFormat.PNG;
        }
        if (s.equals(ImageFileFormat.WEBP)){
            return Bitmap.CompressFormat.WEBP;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    public static Bitmap.CompressFormat getCompressFormat(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return getImageFileCompressFormat(sharedPreferences.getString(PREF_COMPRESSFORMAT,PREF_COMPRESSFORMAT_DEFAULT));
    }

    public static CharSequence getCompressFormatFileExtension(Context context){
        String compressFormat = getImageFileFormatValue(getCompressFormat(context));
        if (compressFormat.equals(ImageFileFormat.PNG)){
            return "png";
        }
        if (compressFormat.equals(ImageFileFormat.WEBP)){
            return "webp";
        }
        return "jpg";
    }

    public static boolean shouldShowHints(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(PREF_SHOWHINTS,PREF_SHOWHINTS_DEFAULT);
    }

    public static void setShouldShowHints(Context context, Boolean b){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(PREF_SHOWHINTS,b);
        pref_editor.apply();
    }

    public static ImageContainer getImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        ImageContainer imageContainer = new ImageContainer();
        imageContainer.uri = Uri.parse(sharedPreferences.getString(PREF_IC_URI,PREF_IC_URI_DEFAULT));
        imageContainer.filesize = sharedPreferences.getLong(PREF_IC_FILESIZE,PREF_IC_FILESIZE_DEFAULT);
        return imageContainer;
    }

    public static void setImageContainer(Context context, Uri uri, long filesize){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_IC_URI,uri.toString());
        pref_editor.putLong(PREF_IC_FILESIZE,filesize);
        pref_editor.apply();
    }

    public static void setImageContainer(Context context, ImageContainer imageContainer){
        setImageContainer(context, imageContainer.uri, imageContainer.filesize);
    }

    public static void resetImageContainer(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(PREF_IC_URI,PREF_IC_URI_DEFAULT);
        pref_editor.putLong(PREF_IC_FILESIZE,PREF_IC_FILESIZE_DEFAULT);
        pref_editor.apply();
    }


    }
