/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

public class PictureCache {

    final static String PICTURECACHE_FILENAME = "picture_cache";
    final static String PICTURECACHE_FILENAME_EXT = ".tmp";

    /*
     * all the stuff here ist static
     */

    private PictureCache() {
    }

    /*
     * Imagepipe picture cache logic:
     *
     * cache is invalidated:
     *     - new image received (done)
     *     - image loaded (done)
     *     - image saved to disk (done)
     *     - image shared (not needed)
     */

    private static File getPictureTempFile(Context context, int position){
        return new File(context.getCacheDir(),PICTURECACHE_FILENAME+position+PICTURECACHE_FILENAME_EXT);
    }

    public static void clearPictureCache(Context context, int position){
        File file=getPictureTempFile(context,position);
        if (file.exists()){
            file.delete();
            clearPictureCache(context,position+1);
        }
    }

    public static void clearPictureCache(Context context){
        clearPictureCache(context,0);
    }

    public static int saveToPictureCache(final Context context, final Bitmap bitmap){
        final int position = getPictureCacheSize(context);
        final File file = getPictureTempFile(context,position);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,out);
                    out.flush();
                    out.close();
                } catch (Exception e){
                    // do nothing
                }
            }
        }).start();
        return position;
    }

    public static Bitmap restoreFromPictureCache(Context context, int position){
        File file = getPictureTempFile(context,position);
        if (file.exists()){
            try {
                FileInputStream inputstream = new FileInputStream(file);
                Bitmap bitmapImmutable = BitmapFactory.decodeStream(inputstream);
                inputstream.close();
                return bitmapImmutable.copy(Bitmap.Config.ARGB_8888,true);
            } catch (Exception e){
                return null;
            }
        }
        return null;
    }

    public static int getPictureCacheSize(Context context){
        int i=0;
        while (getPictureTempFile(context,i).exists()){
            i++;
        }
        return i;
    }

    public static int getLastUsedCachePosition(Context context){
        int cacheSize = getPictureCacheSize(context);
        long lastTime = 0;
        int resultPosition = 0;
        for (int i=0; i<cacheSize; i++){
            File file = getPictureTempFile(context,i);
            long time = file.lastModified();
            if (time>lastTime){
                lastTime = time;
                resultPosition = i;
            }
        }
        return resultPosition;
    }

    public static boolean touchPicture(Context context, int position){
        File file = getPictureTempFile(context,position);
        if (file.exists()){
            return file.setLastModified(Calendar.getInstance().getTimeInMillis());
        }
        return false;
    }
}
