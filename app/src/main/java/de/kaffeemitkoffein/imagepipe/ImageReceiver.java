/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.kaffeemitkoffein.imagepipe;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.*;
import android.graphics.*;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.os.*;
import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import java.io.*;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.Manifest;
import android.content.pm.PackageManager;

/**
 * This class provides all the relevant functions and interaction for Imagepipe.
 * It extends Activity and gives compatibility down to API 1.
 */

public class ImageReceiver extends Activity{

    /**
     * ID string for debugging. Should not occur in logs in any official release.
     */

    private final String IMAGEPIPE_TAG = "IMAGEPIPE-DEBUG";

    /**
     * Constants for callbacks after permissions were granted.
     * The two functions need the read/write to SD storage permission:
     * - select image from gallery
     * - pipe the image
     * - save image to disk
     *
     * missing_permissions_task holds the call type that has to be made once
     * permissions are granted.
     * */

    private static final int PERMISSION_CALLBACK = 98;

    private static final int MISSING_PERMISSIONS_TASK_sendImageUriIntent = 1;
    private static final int MISSING_PERMISSIONS_TASK_pipeTheImage = 2;
    private static final int MISSING_PERMISSIONS_TASK_bulkpipe = 4;
    private static final int MISSING_PERMISSIONS_TASK_saveImage = 3;

    private int missing_permissions_task;

    /**
     * Callback constant to identify the select from gallery task.
     */

    private static final int SELECT_FROM_GALLERY_CALLBACK=99;

    private static final int BUBBLE_RUN_DELAY = 3500;

    private static final int HINT_COUNT = 2;


    /**
     * Saved instance-state constants.
     */

    private static final String SIS_CUT_LINE_TOP="SIS_CUT_LINE_TOP";
    private static final String SIS_CUT_LINE_BOTTOM="SIS_CUT_LINE_BOTTOM";
    private static final String SIS_CUT_LINE_LEFT="SIS_CUT_LINE_LEFT";
    private static final String SIS_CUT_LINE_RIGHT="SIS_CUT_LINE_RIGHT";
    private static final String SIS_SCALE_RATIO="SIS_SCALE_RATIO";
    private static final String SIS_IMAGE_NEEDS_TO_BE_SAVED ="SIS_IMAGE_NEEDS_TO_BE_SAVED";
    private static final String SIS_LAST_IMAGE_URI="SIS_LAST_IMAGE_URI";
    private static final String SIS_NO_IMAGE_LOADED="SIS_NO_IMAGE_LOADED";
    private static final String SIS_JPEG_FILESIZE="SIS_JPEG_FILESIZE";
    private static final String SIS_ORIGINAL_FILESIZE="SIS_ORIGINAL_FILESIZE";
    private static final String SIS_PIPING_ALREADY_LAUNCHED="SIS_PIPING_ALREADY_LAUNCHED";
    private static final String SIS_CACHE_POSITION="SIS_CACHE_POSITION";
    private static final String SIS_PAINTTOOL="SIS_PAINTTOOL";
    private static final String SIS_BRUSHSIZE="SIS_BRUSHSIZE";
    private static final String SIS_SELECTEDCOLOR="SIS_SELECTEDCOLOR";
    private static final String SIS_CHARSEQUENCE="SIS_CHARSEQUENCE";
    private static final String SIS_TEXTSTYLE_BOLD="SIS_TEXTSTYLE_BOLD";
    private static final String SIS_TEXTSTYLE_ITALIC="SIS_TEXTSTYLE_ITALIC";
    private static final String SIS_TEXTSTYLE_UNDERLINE="SIS_TEXTSTYLE_UNDERLINE";
    private static final String SIS_TEXTSTYLE_STRIKETHROUGH="SIS_TEXTSTYLE_STRIKETHROUGH";

    /**
     * Constants for calling the ImagePipeInfo class that displays info text.
     */

    /**
     * Error codes.
     */

    private static final int ERROR_NO_IMAGE_APP = 1;
    private static final int ERROR_NO_BULK_IMAGE_APP = 2;

    private static final String DATA_TITLE="DATA_TITLE";
    private static final String DATA_TEXTRESOURCE="DATA_TEXTRESOURCE";
    private static final String DATA_BUTTONTEXT="DATA_BUTTONTEXT";

    /**
     * Debug string that is displayed above the image info in runtime.
     */

    private String debug = "";

    /**
     * Static filename for file in cache. This file is used to save a stream
     * into a file to get exif data.
     */

    private static final String TEMP_FILENAME="tempimage.jpg";

    /**
     * Variables that say if permissons are available.
     */

    private Boolean hasReadStoragePermission = false;
    private Boolean hasWriteStoragePermission = false;

    /**
     * The key bitmaps the program works with. Concept is:
     * Bitmap image : the visible bitmap with quality preview
     * Bitmap scaled: non-visible backup of the scaled bitmap, but without quality loss. This is
     *                used to calculate "image".
     * Bitmap original: this is a backup of the loaded image. It is "autoscaled" and
     *                  "autorotated".
     */

    /*
     * bitmap_visible  = the visible bitmap with applied quality preview
     * bitmap_image    = the image to be modified / painted on
     * bitmap_original = optinal bitmap holding original image, autoscaled and autorotated. May
     *                   be null in low memory conditions.
     */

    private Bitmap bitmap_visible = null;
    private Bitmap bitmap_image = null;
    private Bitmap bitmap_original = null;

    /**
     * Temporary holds the received intent. This needs to be defined global
     * because it is used in async task.
     */

    private Intent call_intent;

    /**
     * Variable that holds the (main) imageview where the image is displayed.
     */

    private ImageView image_display;
    private EditText edit_size;

    /**
     * The loaded savedInstanceState. Is populated in onCreate.
     */

    private Bundle savedInstanceState;

    /**
     * Local preferences instance.
     * */

    private ImagepipePreferences pref;

    /**
     * Is set true if info bubble is visible.
     */

    private Boolean bubble_visible = false;

    /**
     * Is true if no image was loaded.
     */

    private Boolean no_image_loaded = true;

    /**
     * Indicates if there is a gallery call pending. This is needed to catch
     * simultaneous gallery calls just after permissions were granted.
     */

    private Boolean gallery_already_called = false;

    /**
     * Floats holding the cut-line boundaries globally.
     *
     * "offset_"    : offset coordinates of a gesture to calculate frame changes.
     * scale_ratio  : scale factor of the bitmap inside the imageview (image_display).
     */

    private float cut_line_top;
    private float cut_line_bottom;
    private float cut_line_left;
    private float cut_line_right;
    private float scale_ratio;

    /**
     * x_pos and x_pos hold the last touch position of the 1st touch pointer.
     * x_pos and x_pos hold the last touch position of the 2st touch pointer.
     *
     * Both are used in the cropping feature. They are global to identify movement
     * directions easier.
     */

    float x_pos;
    float y_pos;
    float x1_pos;
    float y1_pos;

    /**
     * Constants for the cropping borders.
     */

    private final static int LEFT_CUTLINE = 0;
    private final static int RIGHT_CUTLINE = 1;
    private final static int TOP_CUTLINE = 2;
    private final static int BOTTOM_CUTLINE = 3;

    /**
     * Coordinates of the "+" button visible when no image is loaded. This is populated
     * on runtime.
     */

    private float x1_choose_button;
    private float x2_choose_button;
    private float y1_choose_button;
    private float y2_choose_button;

    /**
     * Indicates if there is a change that can be saved. Prevents multiple saving of the
     * same image into a file.
     */

    private boolean image_needs_to_be_saved = true;

    /**
     * Holds the uri of the latest saved file/image.
     */

    private ImageContainer lastImageContainer;

    /**
     * Holds the uri, filesize (and optionally file) of the ORIGINAL image
     */

    private ImageContainer originalImageContainer;

    /**
     *
     */

    private ArrayList<ImageContainer> multiplePipeList;

    /**
     * Global context constant for async tasks to refer the app context.
     */

    private Context context = this;

    /**
     * Filesize (preview) of the encoded jpeg file in bytes.
     * 0: no preview available / preview calculation failed.
     */

    private long jpeg_ecoded_filesize = 0;

    /**
     * Time in millis from when on the next jpeg preview is allowed to be calculated.
     * Uses millis since 1970. "0" is a failsave init that allows the next calculation
     * to be made immediately.
     *
     * On runtime, this is counted from time now + NEXT_UPDATE_TIME_DELAY
     */

    private long next_quality_update_time = 0;

    /**
     * Mandatory delay between jpeg calculations in RAM to safe resources.
     * See above.
     */

    private static final int NEXT_UPDATE_TIME_DELAY = 2000;

    private Boolean quality_update_is_dispatched = false;

    /**
     * Is true if there is a quality preview calculation already pending.
     * Prevents multiple calls.
     */
    private Boolean piping_was_already_launched = false;

    /**
     * Original filesize of the loaded image / stream.
     * Is used to calculate compression ratio.
     */

    private long original_image_filesize = 0;

    public ImageReceiver() {
    }

    /**
     * Class to determine the selected painting tool
     */

    class Tool {
        final static int NONE = 0;
        final static int CROP = 1;
        final static int BLUR = 2;
        final static int TEXT = 3;
        final static int PAINT = 4;
    }

    /**
     * selected tool
     */

    private int paintTool = Tool.NONE;
    private int brushSize = -1;
    private int selectedColor = Color.WHITE;
    private int cachePosition = 0;
    private CharSequence charSequence = "";
    private boolean textStyle_bold = false;
    private boolean textStyle_italic = false;
    private boolean textStyle_underline = false;
    private boolean textStyle_strikethrough = false;

    float imageScaleFactor = 1;

    /**
     * static button hooks
     */

    private ImageButton buttonCrop;
    private ImageButton buttonBlur;
    private ImageButton buttonText;
    private ImageButton buttonPaint;
    private ImageButton buttonColor;
    private ImageButton buttonUnDo;
    private ImageButton buttonReDo;
    private ImageButton newCanvas;

    /**
     * static button frame hooks
     */

    private FrameLayout buttonCropFrame;
    private FrameLayout buttonBlurFrame;
    private FrameLayout buttonTextFrame;
    private FrameLayout buttonPaintFrame;
    private FrameLayout buttonColorFrame;

    /*
     * static listeners
     */

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            // simply refresh preferences, no more to do at the moment
            if (!s.equals(ImagepipePreferences.PREF_SCALEMAX)){
                readPreferences();
            }
            if (s.equals(ImagepipePreferences.PREF_COMPRESSFORMAT)){
                setQualitybarVisibility();
            }
        }
    };

    private final View.OnLongClickListener brushSizeListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showBrushSelector();
            return true;
        }
    };

    private final View.OnLongClickListener charSequenceListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            showTextSelector();
            return true;
        }
    };

    private final View.OnClickListener paletteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showPalette();
        }
    };

    private final View.OnClickListener colorButtonListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            colorButtonPressed(view);
        }
    };

    private final View.OnClickListener unDoButtonListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            unDoLastAction();
        }
    };

    private final View.OnClickListener reDoButtonListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            reDoAction();
        }
    };

    private final View.OnClickListener newCanvasListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            createNewCanvas();
        }
    };

    private final View.OnClickListener rotateRightListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!no_image_loaded){
                rotateRight();
            }
        }
    };

    private final View.OnClickListener rotateLeftListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!no_image_loaded){
                rotateLeft();
            }
        }
    };

    private final View.OnClickListener flipHorizontalListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            flipImage(FLIP_HORIZONTALLY);
        }
    };

    private final View.OnLongClickListener flipVerticallyListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            flipImage(FLIP_VERTICALLY);
            return true;
        }
    };

    /*
     * Timestamp when the last modification of the image occurred
     */

    long imageLastModified = Calendar.getInstance().getTimeInMillis();

    @Override
    protected void onCreate(Bundle sic) {
        super.onCreate(sic);
        savedInstanceState = sic;
        setContentView(R.layout.activity_image_receiver);
        imageEditor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        readPreferences();
        updateImageDisplay();
    }

    @Override
    protected void onPause() {
        deleteCacheFile();
        super.onPause();
    }

    @SuppressWarnings("deprecation")
    private void setOverflowMenuItemColor(Menu menu, int id, int string_id,int color_id){
        String s = getApplicationContext().getResources().getString(string_id);
        MenuItem menuItem = menu.findItem(id);
        SpannableString spannableString = new SpannableString(s);
        if (Build.VERSION.SDK_INT>=23){
            spannableString.setSpan(new ForegroundColorSpan(getApplicationContext().getResources().getColor(color_id,getTheme())),0,s.length(),0);
        } else {
            spannableString.setSpan(new ForegroundColorSpan(getApplicationContext().getResources().getColor(color_id)),0,s.length(),0);
        }
        menuItem.setTitle(spannableString);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.image_receiver,menu);
        // try to show icons in drop-down menu
        if (menu.getClass().getSimpleName().equals("MenuBuilder")){
            try {
                Method method = menu.getClass().getDeclaredMethod("setOptionalIconsVisible",Boolean.TYPE);
                method.setAccessible(true);
                method.invoke(menu,true);
            } catch (Exception e){
                // do nothing
            }
        }
        setOverflowMenuItemColor(menu,R.id.menu_add,R.string.gallery_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_share,R.string.share_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_save,R.string.save_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_cleardata,R.string.cleardata_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_settings,R.string.settings_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_exifdialog,R.string.exif_tags, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_about,R.string.about_button, R.color.primaryTextColor);
        setOverflowMenuItemColor(menu,R.id.menu_licence,R.string.licence_button, R.color.primaryTextColor);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
       int item_id = mi.getItemId();
        if (item_id == R.id.menu_add) {
            pickImageFromGallery();
        }
        if (item_id == R.id.menu_share) {
            sendImageUriIntent();
            return true;
        }
        if (item_id == R.id.menu_save) {
            if (!hasReadWriteStorageRuntimePermission()) {
                missing_permissions_task = MISSING_PERMISSIONS_TASK_saveImage;
            } else {
                saveAndScanImage(true);
            }
        }
        if (item_id == R.id.menu_cleardata){
            clearImageData();
        }
        if (item_id == R.id.menu_settings) {
            Intent i = new Intent(this, Settings.class);
            startActivity(i);
            return true;
        }
        if (item_id==R.id.menu_licence) {
            Intent i = new Intent(this, ImagePipeInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.license_title));
            i.putExtra(DATA_TEXTRESOURCE, "license");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.intro_button_text));
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_exifdialog) {
            showExifDialog();
            return true;
        }
        if (item_id == R.id.menu_about) {
            showIntroDialog();
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio){
        if ((!no_image_loaded) && (paintTool==Tool.CROP)){
            Bitmap draw_bitmap;
           try {
                draw_bitmap = bitmap_visible.copy(bitmap_visible.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Canvas canvas = new Canvas();
            canvas.setBitmap(draw_bitmap);
            Paint paint = new Paint();
            paint.setColor(getAppColor(R.color.secondaryLightColor));
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5*scale_ratio);
            // canvas.drawCircle((x_touched-x_padding/2)*scale_ratio,(y_touched-y_padding/2)*scale_ratio,10,paint);
            canvas.drawRect(x1,y1,x2,y2,paint);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
            updateImagePropertiesText();
         }
    }

    /**
     * Draws the cut-frame inside the imageview. Coordinates are absolute values on the bitmap.
     * Debug version for also displaying touch-event coordinates inside the imageview. Not used in
     * the productive code.
     *
     * @param x1 x-coord
     * @param y1 y-coord
     * @param x2 x-coord
     * @param y2 y-coord
     * @param x3 x-coord of touch-event to display
     * @param y3 y-coord of touch-event to display
     * @param scale_ratio Scale ratio of the bitmap inside the imageview.
     */

    private void drawCutFrame(float x1,float y1, float x2, float y2, float scale_ratio, float x3, float y3){
        if ((!no_image_loaded) && (paintTool==Tool.CROP)){
            Bitmap draw_bitmap;
            try {
                draw_bitmap = bitmap_visible.copy(bitmap_visible.getConfig(),true);
            } catch (OutOfMemoryError e){
                // nothing to do; this frame drawing may fail due to low memory.
                return;
            }
            Canvas canvas = new Canvas();
            canvas.setBitmap(draw_bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.CYAN);
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5 * scale_ratio);
            canvas.drawCircle(x3, y3, 10, paint);
            canvas.drawRect(x1, y1, x2, y2, paint);
            if ((image_display.getWidth() !=0) && (image_display.getHeight() != 0)) {
                image_display.setImageBitmap(draw_bitmap);
                image_display.invalidate();
            }
        }
        }

    /**
     * Calculates the new cut-frame-borders from the touch-event.
     * All coordinates are BITMAP coordinates. They were corrected from the view-coordinates by taking the
     * scale ratio into consideration!
     * @param x_delta_image delta-x from the touch-event (swipe)
     * @param y_delta_image delta-y from the touch-event (swipe)
     * @param touch_is_inside_frame indicates if the event is inside the cut-frame (true) or outside (false)
     */

    private void calculateCutFramePosition(float x_delta_image, float y_delta_image, Boolean touch_is_inside_frame){
        if (touch_is_inside_frame) {
            if (x_delta_image < 0)
                cut_line_right = cut_line_right - Math.abs(x_delta_image);
            if (x_delta_image > 0)
                cut_line_left = cut_line_left + Math.abs(x_delta_image);
            if (y_delta_image < 0)
                cut_line_bottom = cut_line_bottom - Math.abs(y_delta_image);
            if (y_delta_image > 0)
                cut_line_top = cut_line_top + Math.abs(y_delta_image);
        } else {
            if (x_delta_image < 0)
                cut_line_left = cut_line_left - Math.abs(x_delta_image);
            if (x_delta_image > 0)
                cut_line_right = cut_line_right + Math.abs(x_delta_image);
            if (y_delta_image < 0)
                cut_line_top = cut_line_top - Math.abs(y_delta_image);
            if (y_delta_image > 0)
                cut_line_bottom = cut_line_bottom + Math.abs(y_delta_image);
        }
        if (cut_line_bottom<cut_line_top){
            float tmp = cut_line_bottom;
            cut_line_bottom = cut_line_top;
            cut_line_top = tmp;
        }
        if (cut_line_right<cut_line_left){
            float tmp = cut_line_right;
            cut_line_right = cut_line_left;
            cut_line_left = tmp;
        }
        fixCropOutOfBounds();
    }

    private void fixCropOutOfBounds(){
        if (cut_line_left<0)
            cut_line_left = 0;
        if (cut_line_right>bitmap_visible.getWidth())
            cut_line_right = bitmap_visible.getWidth();
        if (cut_line_top<0)
            cut_line_top = 0;
        if (cut_line_bottom>bitmap_visible.getHeight())
            cut_line_bottom = bitmap_visible.getHeight();
    }

    /**
     * Calculates the scale ratio of the bitmap inside the imageview.
     * @return float scale-ratio
     */

    private Float getScaleRatio(){
        if ((image_display!=null) && (bitmap_image!=null)) {
            if ((image_display.getWidth()!=0) && (image_display.getHeight()!=0) && (bitmap_image.getWidth()!=0) && (bitmap_image.getHeight()!=0)){
                float view_width = image_display.getWidth();
                float view_height = image_display.getHeight();
                float original_image_width = bitmap_image.getWidth();
                float original_image_height = bitmap_image.getHeight();
                float x_ratio = original_image_width / view_width;
                float y_ratio = original_image_height / view_height;
                if ( y_ratio > x_ratio ){
                    return y_ratio;
                } else {
                    return x_ratio;
                }
            }
        }
        return 1f;
    }

    /**
     * Helper sub to get a color from resource.
     * @param color_resource is the R.id.color of the color.
     * @return int color value from a resource
     */

    @SuppressWarnings("deprecation")
    private int getAppColor(int color_resource){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            try {
                return getResources().getColor(color_resource, this.getTheme());
            } catch ( Exception e ) {
                return 0;
            }
            }
            else {
            try {
                return getResources().getColor(color_resource);
            } catch ( Exception e ) {
                return 0;
            }
            }
    }

    /**
     * A runnable that updates the image_display view with the image bitmap. This is only
     * a helper of the updateImageDisplay() call.
     */

    private void updateImageDisplay_runnable(){
        bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
        image_display.setImageBitmap(bitmap_visible);
        if ((pref.previewquality) && (!pref.compressformat.equals(ImagepipePreferences.ImageFileFormat.PNG))){
            displayImageQuality();
        }
    }

    /**
     * Updates the image_display view with the image bitmap.
     * Checks if the view is already visible. If not, the call is queued.
     */

    private void updateImageDisplay(){
        if (bitmap_image!=null){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateImageDisplay_runnable();
                }
            });
        } else {
            setEmptyDefaultScreen();
        }
    }

    private Boolean touchIsInsideFrame(float x_touched_image, float y_touched_image){
        if (    (x_touched_image > cut_line_left) &&
                (x_touched_image < cut_line_right) &&
                (y_touched_image > cut_line_top) &&
                (y_touched_image < cut_line_bottom)){
            return true;
        } else {
            return false;
        }
    }

    private int determineCutlinePointer(float x0,float y0,float x1,float y1, int pointer){
        if (pointer==LEFT_CUTLINE){
            if (x0<=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==RIGHT_CUTLINE){
            if (x0>=x1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==TOP_CUTLINE){
            if (y0<=y1) {
                return 0;
            } else {
                return 1;
            }
        }
        if (pointer==BOTTOM_CUTLINE)
            if (y0>=y1) {
                return 0;
            } else {
                return 1;
            }
        return 0;
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me, int historyindex){
        float x_padding = (v.getWidth()-bitmap_visible.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-bitmap_visible.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        float y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getHistoricalX(i,historyindex) - x_padding) * scale_ratio;
        y_value = (me.getHistoricalY(i,historyindex) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;

        fixCropOutOfBounds();
    }

    private void calculateCutFrameMultitouch(View v, MotionEvent me){
        float x_padding = (v.getWidth()-bitmap_visible.getWidth()/scale_ratio)/2;
        float y_padding = (v.getHeight()-bitmap_visible.getHeight()/scale_ratio)/2;

        int i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),LEFT_CUTLINE);
        float x_value = (me.getX(i) - x_padding) * scale_ratio;
        float y_value = (me.getY(i) - y_padding) * scale_ratio;
        float x_delta = x_value - x_pos;
        float y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_left = cut_line_left + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),RIGHT_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_right = cut_line_right + x_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),TOP_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_top = cut_line_top + y_delta;

        i = determineCutlinePointer(me.getX(0), me.getY(0),me.getX(1),me.getY(1),BOTTOM_CUTLINE);
        x_value = (me.getX(i) - x_padding) * scale_ratio;
        y_value = (me.getY(i) - y_padding) * scale_ratio;
        x_delta = x_value - x_pos;
        y_delta = y_value - y_pos;
        if (i==1) {
            x_delta = x_value - x1_pos;
            y_delta = y_value - y1_pos;
        }
        cut_line_bottom = cut_line_bottom + y_delta;
        fixCropOutOfBounds();
    }

    private void setPaintTool(int tool){
        paintTool = tool;
        int cropColor = Color.TRANSPARENT;
        int blurColor = Color.TRANSPARENT;
        int textColor = Color.TRANSPARENT;
        int paintColor = Color.TRANSPARENT;
        int colorColor = Color.TRANSPARENT;
        switch (paintTool){
            case Tool.CROP: cropColor = getAppColor(R.color.colorAccent); break;
            case Tool.BLUR: blurColor = getAppColor(R.color.colorAccent); break;
            case Tool.TEXT: textColor = getAppColor(R.color.colorAccent); break;
            case Tool.PAINT: paintColor = getAppColor(R.color.colorAccent);
        }
        buttonCropFrame.setBackgroundColor(cropColor);
        buttonBlurFrame.setBackgroundColor(blurColor);
        buttonTextFrame.setBackgroundColor(textColor);
        buttonPaintFrame.setBackgroundColor(paintColor);
        // remove visible cut-frames if tool is not crop
        if (tool!=Tool.CROP){
            updateImageDisplay();
        }
    }

    private void registerPaintToolButtonListeners(){
        buttonCropFrame = (FrameLayout) findViewById(R.id.tools_crop_frame);
        buttonBlurFrame = (FrameLayout) findViewById(R.id.tools_blur_frame);
        buttonTextFrame = (FrameLayout) findViewById(R.id.tools_text_frame);
        buttonPaintFrame = (FrameLayout) findViewById(R.id.tools_paint_frame);
        buttonColorFrame = (FrameLayout) findViewById(R.id.tools_color_frame);
        buttonCrop  = (ImageButton) findViewById(R.id.tools_crop);
        buttonBlur  = (ImageButton) findViewById(R.id.tools_blur);
        buttonText  = (ImageButton) findViewById(R.id.tools_text);
        buttonPaint = (ImageButton) findViewById(R.id.tools_paint);
        buttonColor = (ImageButton) findViewById(R.id.tools_color);
        buttonUnDo = (ImageButton) findViewById(R.id.tools_undo);
        buttonReDo = (ImageButton) findViewById(R.id.tools_redo);
        buttonCrop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paintTool==Tool.CROP){
                    crop();
                } else {
                    setPaintTool(Tool.CROP);
                }
            }
        });
        buttonBlur.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.BLUR);
                if (brushSize<0){
                    brushSize = 25;
                    showBrushSelector();
                }
            }
        });
        buttonBlur.setOnLongClickListener(brushSizeListener);
        buttonText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.TEXT);
                if (charSequence.equals("")){
                    showTextSelector();
                }
            }
        });
        buttonText.setOnLongClickListener(charSequenceListener);
        buttonPaint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaintTool(Tool.PAINT);
                if (brushSize<0){
                    brushSize = 25;
                    showBrushSelector();
                }
            }
        });
        buttonPaint.setOnLongClickListener(brushSizeListener);
        buttonColor.setOnClickListener(paletteListener);
        buttonUnDo.setOnClickListener(unDoButtonListener);
        buttonReDo.setOnClickListener(reDoButtonListener);
    }

    private void setLastModifiedTimestamp(){
        imageLastModified = Calendar.getInstance().getTimeInMillis();
        image_needs_to_be_saved = true;
    }

    private int realBrushSize(){
        return brushSize;
    }

    private int realBlurSize(){
        return brushSize;
    }

    public void paintBrushApply(float x, float y,final Canvas canvas){
        Paint paint = new Paint();
        paint.setColor(selectedColor);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawCircle(x,y,realBrushSize()/2f,paint);
    }

    public void paintBrush(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        paintBrushApply(x,y,image);
        paintBrushApply(x,y,visible);
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void blurBrushApply(float x, float y,final Canvas canvas, int color){
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        paint.setMaskFilter(new BlurMaskFilter(realBrushSize(),BlurMaskFilter.Blur.NORMAL));
        canvas.drawCircle(x,y,realBlurSize()/2f,paint);
    }

    public int getBlurColor(final Bitmap bitmap, final int x, final int y){
        ArrayList<Integer> colors = new ArrayList<Integer>();
        try {
            colors.add(bitmap.getPixel(x,y));
            colors.add(bitmap.getPixel(x-1,y));
            colors.add(bitmap.getPixel(x+1,y));
            colors.add(bitmap.getPixel(x,y-1));
            colors.add(bitmap.getPixel(x,y+1));
        } catch (IllegalArgumentException e){
            if (colors.size()<1){
                colors.add(Color.BLACK);
            }
        }
        int red = 0;
        int green = 0;
        int blue = 0;
        for (int i=0; i<colors.size(); i++){
            red = red + Color.red(colors.get(i));
            green = green + Color.green(colors.get(i));
            blue = blue + Color.blue(colors.get(i));
        }
        red = red/(colors.size());
        green = green/(colors.size());
        blue = blue/(colors.size());
        int blurColor = Color.argb(255,red,green,blue);
        return blurColor;
    }

    public void blurBrush(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        int color = getBlurColor(bitmap_image,Math.round(x),Math.round(y));
        blurBrushApply(x,y,image,color);
        blurBrushApply(x,y,visible,color);
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void putTextApply(float x, float y, final Canvas canvas, int textsize){
        Paint paint = new Paint();
        paint.setColor(selectedColor);
        paint.setAntiAlias(true);
        paint.setTextSize(textsize);;
        if (textStyle_bold){
            paint.setFakeBoldText(true);
        }
        if (textStyle_italic){
            paint.setTextSkewX((float) -0.25);
        }
        if (textStyle_underline){
            paint.setUnderlineText(true);
        }
        if (textStyle_strikethrough){
            paint.setStrikeThruText(true);
        }
        canvas.drawText(String.valueOf(charSequence),x,y,paint);
    }

    public void putText(float x, float y){
        Canvas image = new Canvas(bitmap_image);
        Canvas visible = new Canvas(bitmap_visible);
        int textsize = realBrushSize();
        Paint paint = new Paint();
        // decrease font size to fit text in position
        paint.setTextSize(textsize);
        while ((textsize>6) && (paint.measureText(String.valueOf(charSequence))>bitmap_image.getWidth()-x)){
            textsize--;
            paint.setTextSize(textsize);
        }
        putTextApply(x,y,image,textsize);
        putTextApply(x,y,visible,textsize);
        image_display.setImageBitmap(bitmap_visible);
        setLastModifiedTimestamp();
    }

    public void crop(){
        if (!no_image_loaded){
            if ((cut_line_left<cut_line_right) && (cut_line_top<cut_line_bottom)) {
                try {
                    bitmap_image = Bitmap.createBitmap(bitmap_image, Math.round(cut_line_left), Math.round(cut_line_top), Math.round(cut_line_right - cut_line_left), Math.round(cut_line_bottom - cut_line_top));
                    setLastModifiedTimestamp();
                    putImageBitmapToCache();
                    updateImageDisplay();
                    image_needs_to_be_saved = true;
                    updateImagePropertiesText();
                    resetCutFrame();
                } catch (Exception e) {
                    // simply nothing to do if cut failed for some reason.
                }
            }
        }
    }

    public void rotateRight(){
        if (rotateImage(90)) {
            float cut_line_bottom_new = cut_line_right;
            float cut_line_top_new = cut_line_left;
            float cut_line_left_new = bitmap_image.getWidth() - cut_line_bottom;
            float cut_line_right_new = bitmap_image.getWidth() - cut_line_top;
            cut_line_bottom = cut_line_bottom_new;
            cut_line_top = cut_line_top_new;
            cut_line_left = cut_line_left_new;
            cut_line_right = cut_line_right_new;
            putImageBitmapToCache();
            updateImageDisplay();
            drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
        }
    }

    public void rotateLeft(){
        if (rotateImage(-90)) {
            float cut_line_bottom_new = bitmap_image.getHeight() - cut_line_left;
            float cut_line_top_new = bitmap_image.getHeight() - cut_line_right;
            float cut_line_left_new = cut_line_top;
            float cut_line_right_new = cut_line_bottom;
            cut_line_bottom = cut_line_bottom_new;
            cut_line_top = cut_line_top_new;
            cut_line_left = cut_line_left_new;
            cut_line_right = cut_line_right_new;
            putImageBitmapToCache();
            updateImageDisplay();
            drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
        }
    }

    public void setQualitybarVisibility(){
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar_quality);
        TextView textView = (TextView) findViewById(R.id.quality_text);
        if (pref.compressformat.equals(ImagepipePreferences.ImageFileFormat.PNG)) {
            if (seekBar!=null){
                seekBar.setVisibility(View.INVISIBLE);
            }
            if (textView!=null){
                textView.setVisibility(View.INVISIBLE);
            }
        } else {
            if (seekBar!=null){
                seekBar.setVisibility(View.VISIBLE);
            }
            if (textView!=null){
                textView.setVisibility(View.VISIBLE);
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void imageEditor(){
        readPreferences();
        pref.sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);
        registerPaintToolButtonListeners();
        call_intent = getIntent();
        String i_action = call_intent.getAction();
        String i_type = call_intent.getType();
        image_display = (ImageView) findViewById(R.id.pipedimage);
        // disable hardware acceleration for this view because blur is not supported with hardware acc.
        image_display.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        image_display.setOnTouchListener(new ImageView.OnTouchListener(){
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
            public boolean onTouch(View v, MotionEvent me){
               // hide floating menus if visible
                hideToolsIfVisible();
               if (bubble_visible){
                   updateImagePropertiesText();
               }
               if (no_image_loaded){
                   if (!gallery_already_called)
                       if ((me.getX()>=x1_choose_button) && (me.getX()<=x2_choose_button) &&
                               (me.getY()>=y1_choose_button) && (me.getY()<=y2_choose_button)){
                       pickImageFromGallery();
                       gallery_already_called = true;
                       return true;
                        }
                   return false;
               }
               int action = me.getActionMasked();
               int pid    = me.getActionIndex();
                scale_ratio=getScaleRatio();
                float x_padding = (v.getWidth()-bitmap_image.getWidth()/scale_ratio)/2;
                float y_padding = (v.getHeight()-bitmap_image.getHeight()/scale_ratio)/2;
                for (int i=0; i<me.getPointerCount();i++) {
                    if ((me.getX(i) > x_padding) && (me.getX(i) < v.getWidth() - x_padding) &&
                            (me.getY(i) > y_padding) && (me.getY(i) < v.getHeight() - y_padding)) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }
               if (action==MotionEvent.ACTION_DOWN){
                   x_pos = (me.getX() - x_padding) * scale_ratio;
                   y_pos = (me.getY() - y_padding) * scale_ratio;
                   if (paintTool==Tool.CROP){
                       drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                   }
                   if (paintTool==Tool.PAINT){
                       paintBrush(x_pos,y_pos);
                   }
                   if (paintTool==Tool.BLUR){
                       blurBrush(x_pos,y_pos);
                   }
                   if (paintTool==Tool.TEXT){
                       putText(x_pos,y_pos);
                   }
                   return true;
               }
            if (action==MotionEvent.ACTION_POINTER_DOWN){
                x1_pos = (me.getX() - x_padding) * scale_ratio;
                y1_pos = (me.getY() - y_padding) * scale_ratio;
                if (paintTool==Tool.CROP){
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                }
                return true;
            }
            if (action==MotionEvent.ACTION_MOVE) {
                if (me.getPointerCount() == 1) {
                    if (me.getHistorySize() > 0) {
                        x_pos = (me.getHistoricalX(0) - x_padding) * scale_ratio;
                        y_pos = (me.getHistoricalY(0) - y_padding) * scale_ratio;
                        int i = 0;
                        while (i < me.getHistorySize() - 2) {
                            i = i + 1;
                            float x_pos1 = (me.getHistoricalX(i) - x_padding) * scale_ratio;
                            float y_pos1 = (me.getHistoricalY(i) - y_padding) * scale_ratio;
                            float x_delta = x_pos1 - x_pos;
                            float y_delta = y_pos1 - y_pos;
                            if (paintTool==Tool.CROP){
                                calculateCutFramePosition(x_delta, y_delta, touchIsInsideFrame(x_pos, y_pos));
                                drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                            }
                            if (paintTool==Tool.PAINT){
                                paintBrush(x_pos1,y_pos1);
                            }
                            if (paintTool==Tool.BLUR){
                                blurBrush(x_pos1,y_pos1);
                            }
                            x_pos = x_pos1;
                            y_pos = y_pos1;
                        }
                    }
                    float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                    float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                    float x_delta = x_pos1 - x_pos;
                    float y_delta = y_pos1 - y_pos;
                    if (paintTool==Tool.CROP){
                        calculateCutFramePosition(x_delta, y_delta, touchIsInsideFrame(x_pos, y_pos));
                        drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                    }
                    if (paintTool==Tool.PAINT){
                        paintBrush(x_pos1,y_pos1);
                    }
                    if (paintTool==Tool.BLUR){
                        blurBrush(x_pos1,y_pos1);
                    }
                    x_pos = x_pos1;
                    y_pos = y_pos1;
                } else {
                    if (me.getHistorySize() > 0) {
                        x_pos = (me.getHistoricalX(0,0) - x_padding) * scale_ratio;
                        y_pos = (me.getHistoricalY(0,0) - y_padding) * scale_ratio;
                        x1_pos = (me.getHistoricalX(1,0) - x_padding) * scale_ratio;
                        y1_pos = (me.getHistoricalY(1,0) - y_padding) * scale_ratio;
                        int i = 0;
                        while (i < me.getHistorySize() - 2) {
                            i = i + 1;
                            float x_pos1 = (me.getHistoricalX(0, i) - x_padding) * scale_ratio;
                            float y_pos1 = (me.getHistoricalY(0, i) - y_padding) * scale_ratio;
                            float x1_pos1 = (me.getHistoricalX(1, i) - x_padding) * scale_ratio;
                            float y1_pos1 = (me.getHistoricalY(1, i) - y_padding) * scale_ratio;
                            float x_delta = x_pos1 - x_pos;
                            float y_delta = y_pos1 - y_pos;
                            float x1_delta = x1_pos1 - x1_pos;
                            float y1_delta = y1_pos1 - y1_pos;
                            if (paintTool==Tool.CROP){
                                calculateCutFrameMultitouch(v, me, i);
                                drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                            }
                            x_pos = x_pos1;
                            y_pos = y_pos1;
                            x1_pos = x1_pos1;
                            y1_pos = y1_pos1;
                        }
                    }
                    float x_pos1 = (me.getX(0) - x_padding) * scale_ratio;
                    float y_pos1 = (me.getY(0) - y_padding) * scale_ratio;
                    float x1_pos1 = (me.getX(1) - x_padding) * scale_ratio;
                    float y1_pos1 = (me.getY(1) - y_padding) * scale_ratio;
                    if (paintTool==Tool.CROP){
                        calculateCutFrameMultitouch(v, me);
                        drawCutFrame(cut_line_left, cut_line_top, cut_line_right, cut_line_bottom, scale_ratio);
                    }
                    x_pos = x_pos1;
                    y_pos = y_pos1;
                    x1_pos = x1_pos1;
                    y1_pos = y1_pos1;
                }
                return true;
            }
            if (action==MotionEvent.ACTION_UP){
                x_pos = (me.getX() - x_padding) * scale_ratio;
                y_pos = (me.getY() - y_padding) * scale_ratio;
                float x_pos1 = (me.getX() - x_padding) * scale_ratio;
                float y_pos1 = (me.getY() - y_padding) * scale_ratio;
                float x_delta = x_pos1 - x_pos;
                float y_delta = y_pos1 - y_pos;
                if (paintTool==Tool.CROP){
                    calculateCutFramePosition(x_delta,y_delta,touchIsInsideFrame(x_pos,y_pos));
                    drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                    if (pref.smartcrop){
                        crop();
                    }
                }
                if ((paintTool==Tool.PAINT) || (paintTool==Tool.BLUR) || (paintTool==Tool.TEXT)){
                    // as drawing step finished => save image to cache
                    putImageBitmapToCache();
                }
                return true;
            }
            return false;
        }
        });
        // landscape
        Button button_rotateleft = (Button) findViewById(R.id.rotateleft_button);
        Button button_rotateright = (Button) findViewById(R.id.rotateright_button);
        Button button_flip = (Button) findViewById(R.id.flip_button);
        if (button_rotateleft!=null){
            button_rotateleft.setOnClickListener(rotateLeftListener);
        }
        if (button_rotateright!=null){
            button_rotateright.setOnClickListener(rotateRightListener);
        }
        if (button_flip!=null){
            button_flip.setOnClickListener(flipHorizontalListener);
            button_flip.setOnLongClickListener(flipVerticallyListener);
        }
        // portrait
        ImageButton imageButton_rotateleft = (ImageButton) findViewById(R.id.rotateleft_imagebutton);
        ImageButton imageButton_rotateright = (ImageButton) findViewById(R.id.rotateright_imagebutton);
        ImageButton imageButton_flip = (ImageButton) findViewById(R.id.flip_imagebutton);
        if (imageButton_rotateleft!=null){
            imageButton_rotateleft.setOnClickListener(rotateLeftListener);
        }
        if (imageButton_rotateright!=null){
            imageButton_rotateright.setOnClickListener(rotateRightListener);
        }
        if (imageButton_flip!=null){
            imageButton_flip.setOnClickListener(flipHorizontalListener);
            imageButton_flip.setOnLongClickListener(flipVerticallyListener);
        }
        // landscape & portrait
        Button button_processimage = (Button) findViewById(R.id.process_button);
        Button button_reloadimage = (Button) findViewById(R.id.reload_button);
        button_processimage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!no_image_loaded){
                    scaleImage();
                    putImageBitmapToCache();
                    updateImageDisplay();
                }
            }
        });
        button_reloadimage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadImage();
                updateImageDisplay();
            }
        });
        if (savedInstanceState != null) {
            image_needs_to_be_saved = savedInstanceState.getBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED, true);
            no_image_loaded = savedInstanceState.getBoolean(SIS_NO_IMAGE_LOADED, true);
            lastImageContainer = new ImageContainer();
            lastImageContainer.uri = savedInstanceState.getParcelable(SIS_LAST_IMAGE_URI);
            jpeg_ecoded_filesize = savedInstanceState.getLong(SIS_JPEG_FILESIZE);
            original_image_filesize = savedInstanceState.getLong(SIS_ORIGINAL_FILESIZE);
            paintTool = savedInstanceState.getInt(SIS_PAINTTOOL);
            cachePosition = savedInstanceState.getInt(SIS_CACHE_POSITION);
            brushSize = savedInstanceState.getInt(SIS_BRUSHSIZE);
            selectedColor = savedInstanceState.getInt(SIS_SELECTEDCOLOR);
            charSequence = savedInstanceState.getCharSequence(SIS_CHARSEQUENCE);
            textStyle_bold = savedInstanceState.getBoolean(SIS_TEXTSTYLE_BOLD);
            textStyle_italic = savedInstanceState.getBoolean(SIS_TEXTSTYLE_ITALIC);
            textStyle_underline = savedInstanceState.getBoolean(SIS_TEXTSTYLE_UNDERLINE);
            textStyle_strikethrough = savedInstanceState.getBoolean(SIS_TEXTSTYLE_STRIKETHROUGH);
            buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
            if (lastImageContainer.uri != null) {
                // restore image from cache
                if (!readImageBitmapFromCache(cachePosition)){
                    // re-load if restore from cache fails
                    if (!readImageBitmapFromUri(lastImageContainer.uri)){
                            showLoadingFailedErrorToast();
                    }
                }
                getSavedInstanceStateCutLine(savedInstanceState);
            }
            if (bitmap_image != null) {
                 initOriginalBitmap();
                } else {
                no_image_loaded = true;
            }
            if (no_image_loaded) {
                setEmptyDefaultScreen();
            } else {
                updateImageDisplay();
                updateImagePropertiesText();
            }
            } else {
            if ((Intent.ACTION_SEND.equals(i_action)) && (i_type != null)) {
                // clear picture cache because new image received
                clearCache();
                pipeTheImage(call_intent);
                } else {
                    if ((Intent.ACTION_SEND_MULTIPLE.equals(i_action)) && (i_type != null)) {
                        // clear picture cache & resources because new image received
                        clearImageData();
                        // receive multiple images
                        pipeMultipleImages(call_intent);
                    } else {
                        // app seems to be lauched natively: no savedInstanceState, no send, no send_multiple
                        // try to load someting from cache
                        // may result in bitmaps == null
                        readLastImageFromCacheAndRestoreCache();
                    }
                }
            }
        if (no_image_loaded){
            setEmptyDefaultScreen();
            checkForHintBubbleDisplay();
        }
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        setQualitybarVisibility();
        seekbar_quality.setMax(pref.getQualitymaxvalue());
        seekbar_quality.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateImagePropertiesText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // to do
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                writePreferences();
                displayImageQuality();
            }
        });

       edit_size = (EditText) findViewById(R.id.editText_size);
        edit_size.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                writePreferences();
            }
        });
    }

    private int getExifRotationDataFromMediaStore(Uri image_uri){
        String [] imageColumns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor c = this.getContentResolver().query(image_uri,imageColumns,null,null,null);
        if (c == null)
            return -1;
        if (c.moveToFirst()){
            try {
                int i = c.getColumnIndex(imageColumns[1]);
                if (i != -1){
                    int degrees = c.getInt(i);
                    return degrees;
                } else {
                    // orientation-data does not exist.
                    return -1;
                }
            } catch (Exception e) {
                // some other data read error from media store
                return -1;
            }
        }
        c.close();
        return -1;
    }

    private int getExifRotationFromUri(Uri image_uri){
        // try to get rotation from system MediaStore
        int degrees = getExifRotationDataFromMediaStore(image_uri);
        if (degrees !=  -1) {
            return degrees;
        }
        // try to copy stream to file instead
        File tempfile = new File(this.getCacheDir(), TEMP_FILENAME);
        if (tempfile.exists()){
            if (tempfile.length()!=0){
                ExifInterface exif_data;
                try {
                    exif_data = new ExifInterface(tempfile.getAbsolutePath());
                } catch (Exception e){
                    // reading exif data from file failed, no rotation => returns 0
                    return 0;
                }
                switch (exif_data.getAttributeInt(ExifInterface.TAG_ORIENTATION,0)) {
                    case ExifInterface.ORIENTATION_ROTATE_270: degrees = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180: degrees = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:  degrees = 90;
                        break;
                    default: degrees = 0;
                        break;
                }
                return degrees;
            }
            // delete temp file in cache if it exists.
            tempfile.delete();
        }
        // return 0 if the temporary file in cache does not exist.
        return 0;
    }

    private boolean deleteCacheFile(){
        File tempfile = new File(this.getCacheDir(), TEMP_FILENAME);
        return tempfile.delete();
    }

    @SuppressWarnings("deprecation")
    private int getScreenDownscaleValue(){
        Display d = getWindowManager().getDefaultDisplay();
        int height;
        int width;
        if (android.os.Build.VERSION.SDK_INT < 13) {
            width = d.getWidth();
            height = d.getHeight();
        } else {
            Point po = new Point();
            d.getSize(po);
            width = po.x;
            height = po.y;
        }
        if (width > height)
            return width;
        else
            return height;
    }

    private void forceImageSizeDownscale() {
        int max_diameter_display = getScreenDownscaleValue();
        int max_diameter_image;
        if (bitmap_image.getWidth() > bitmap_image.getHeight())
            max_diameter_image = bitmap_image.getWidth();
        else
            max_diameter_image = bitmap_image.getHeight();
        if (max_diameter_image > max_diameter_display)
            scaleImage(max_diameter_display);
    }

    private void applyChangesOnLoadedImage(Uri source_uri){
        next_quality_update_time = 0;
        if (!no_image_loaded){
            if (pref.autoscale){
                scaleImage();
            }
            if (pref.forcedownscale){
                forceImageSizeDownscale();
            }
            if (pref.autorotate) {
                rotateImage(getExifRotationFromUri(source_uri));
            }
            // delete temp image file in cache if it exists.
            deleteCacheFile();
            resetCutFrame();
            bitmap_original = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
            clearCache();
            putImageBitmapToCache();
            updateImageDisplay();
        }
    }

    private Boolean pipeTheImage(Intent intent){
        Uri source_uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_pipeTheImage;
            setEmptyDefaultScreen();
            return false;
        }
        if ( source_uri != null ) {
            if (readImageBitmapFromUri(source_uri)) {
                applyChangesOnLoadedImage(source_uri);
                if ((pref.autopipe) && (!piping_was_already_launched)) {
                        piping_was_already_launched = true;
                        sendImageUriIntent();
                }
                } else {
                showLoadingFailedErrorToast();
                setEmptyDefaultScreen();
                }
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    /**
     * Extracts the uris from an arraylist of ImageContainers into an uri arraylist.
     * Returns null if the parameter is null or if the arraylist is empty.
     *
     * @param imageContainers
     * @return ArrayList<Uri>
     */

    private ArrayList<Uri> getUriArrayList(ArrayList<ImageContainer> imageContainers){
        if (imageContainers!=null) {
            if (imageContainers.size()>0) {
                ArrayList<Uri> urilist = new ArrayList<Uri>();
                for (int i=0; i<imageContainers.size(); i++){
                    if (imageContainers.get(i).uri != null){
                        urilist.add(imageContainers.get(i).uri);
                    } else {
                        // do nothing
                    }
                }
                return urilist;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private void processOneImageForMultiplePipe(final ArrayList<Uri> source_uris, final int i){
        if (i<source_uris.size()){
            if (readImageBitmapFromUri(source_uris.get(i))){
                final Context this_context = context;
                applyChangesOnLoadedImage(source_uris.get(i));
                final ImageContainer imageContainer = saveImage(false);
                if (imageContainer != null) {
                    if (imageContainer.file != null) {
                        FileMediaScanner fms = new FileMediaScanner(this_context,imageContainer.file,false){
                            @Override
                            public void onScanCompleted(String s, Uri uri){
                                mediaScannerConnection.disconnect();
                                imageContainer.uri = uri;
                                imageContainer.file = new File(s);
                                multiplePipeList.add(imageContainer);
                                lastImageContainer = imageContainer;
                                processOneImageForMultiplePipe(source_uris,i+1);
                            }
                        };
                    }
                }
            } else {
                showLoadingFailedErrorToast();
                // however, do not give up but try with next image
                processOneImageForMultiplePipe(source_uris,i+1);
            }
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,getUriArrayList(multiplePipeList));
            intent.setType("image/jpeg");
            if (intent.resolveActivity(this.getPackageManager()) != null){
                try {
                    startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_images_part1)+String.valueOf(i)+getResources().getString(R.string.send_images_part2)));
                } catch (ActivityNotFoundException e){
                    displayNoActivityFoundToast(ERROR_NO_BULK_IMAGE_APP);
                }
            } else {
                displayNoActivityFoundToast(ERROR_NO_BULK_IMAGE_APP);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // finally put last image to cache, store last image uri & update displays
                    ImagepipePreferences.setImageContainer(context,lastImageContainer.uri,original_image_filesize);
                    // also update local preferences
                    pref.ic_filesize = original_image_filesize;
                    pref.ic_uri = lastImageContainer.uri.toString();
                    putImageBitmapToCache();
                    updateImageDisplay();
                    updateImagePropertiesText();
                }
            });
        }
    }

    private void displayNoActivityFoundToast(int error){
        switch (error) {
            case ERROR_NO_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_image_app_error),Toast.LENGTH_LONG).show(); break;
            case ERROR_NO_BULK_IMAGE_APP: Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.no_bulk_image_app_error),Toast.LENGTH_LONG).show(); break;
        }
    }

    private Boolean pipeMultipleImages(Intent intent){
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_bulkpipe;
            setEmptyDefaultScreen();
            return false;
        }
        ArrayList<Uri> source_uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (source_uris != null) {
            multiplePipeList = new ArrayList<ImageContainer>();
            processOneImageForMultiplePipe(source_uris,0);
        } else {
            setEmptyDefaultScreen();
        }
        return true;
    }

    private void reloadImage(){
        if (!no_image_loaded) {
            // try to restore from uri
            if (readImageBitmapFromUri(originalImageContainer.uri)){
                applyChangesOnLoadedImage(originalImageContainer.uri);
                lastImageContainer = ImagepipePreferences.getImageContainer(context);
                originalImageContainer = new ImageContainer(lastImageContainer);
                // clear image cache and restore original to first position
                clearCache();
                putImageBitmapToCache();
                image_needs_to_be_saved = true;
                updateImagePropertiesText();
                resetCutFrame();
                initOriginalBitmap();
            } else {
                // if loading bitmap fails, try to restore from 1st cache position
                Bitmap bitmap = PictureCache.restoreFromPictureCache(context,0);
                if (bitmap!=null){
                    bitmap_original = bitmap;
                    bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
                    bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
                    PictureCache.clearPictureCache(context,1);
                    cachePosition = 0;
                    updateImageDisplay();
                    updateImagePropertiesText();
                    image_needs_to_be_saved = true;
                    resetCutFrame();
                    lastImageContainer = new ImageContainer(Uri.fromParts("file:",NEWCANVAS_AUTHORITY,""),bitmap_original.getByteCount());
                } else {
                    // try to restore from bitmap in memory
                    if (bitmap_original!=null){
                        bitmap_image = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
                        bitmap_visible = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
                        lastImageContainer = new ImageContainer(Uri.fromParts("file:",NEWCANVAS_AUTHORITY,""),bitmap_original.getByteCount());
                        clearCache();
                        putImageBitmapToCache();
                        updateImageDisplay();
                        updateImagePropertiesText();
                    } else {
                        showLoadingFailedErrorToast();
                    }
                }
            }
        }
    }

    private boolean scaleImage(int maxsize){
        if (!no_image_loaded){
            int target_width;
            int target_height;
            if (maxsize == 0){
                    maxsize = Integer.parseInt(pref.scalemax);
            }
            if (bitmap_image.getHeight() > bitmap_image.getWidth()){
                    target_height = maxsize;
                    imageScaleFactor = Float.valueOf(target_height) / Float.valueOf(bitmap_image.getHeight());
                    target_width = Math.round(Float.valueOf(target_height) / Float.valueOf(bitmap_image.getHeight()) * Float.valueOf(bitmap_image.getWidth()));
            } else {
                    target_width = maxsize;
                    imageScaleFactor = Float.valueOf(target_width) / Float.valueOf(bitmap_image.getWidth());
                    target_height= Math.round(Float.valueOf(target_width) / Float.valueOf(bitmap_image.getWidth()) * Float.valueOf(bitmap_image.getHeight()));
            }
            if (target_height<bitmap_image.getHeight() || target_width<bitmap_image.getWidth()) {
                try {
                    // perform operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                    Bitmap bitmapTemp = Bitmap.createScaledBitmap(bitmap_image, target_width, target_height, false);
                    if (bitmapTemp!=null){
                        bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                    }
                    image_needs_to_be_saved = true;
                } catch (OutOfMemoryError e){
                    return false;
                }
                }
            }
        return true;
    }

    private void scaleImage(){
        scaleImage(0);
    }

    private Boolean rotateImage(float degree){
        if (degree == 0){
            return true;
        }
        if (!no_image_loaded){
            Matrix matrix = new Matrix();
            matrix.postRotate(degree);
            try {
                // perform bitmap operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                Bitmap bitmapTemp = Bitmap.createBitmap(bitmap_image,0,0,bitmap_image.getWidth(),bitmap_image.getHeight(),matrix,true);
                if (bitmapTemp!=null){
                    bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                }
                updateImageDisplay();
                image_needs_to_be_saved = true;
                updateImagePropertiesText();
            } catch (OutOfMemoryError e){
                return false;
            }
        }
        return true;
    }

    private class processJpegInBackground extends AsyncTask<Integer,Void,Boolean>{
        ByteArrayOutputStream out_stream;
        long launchTime;
        protected Boolean doInBackground(Integer... compression){
            launchTime = Calendar.getInstance().getTimeInMillis();
            if (bitmap_image != null){
                try {
                    out_stream = new ByteArrayOutputStream();
                } catch (OutOfMemoryError e) {
                    return false;
                }
                bitmap_visible = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                try {
                    bitmap_visible.compress(ImagepipePreferences.getCompressFormat(context), compression[0], out_stream);
                } catch (OutOfMemoryError e){
                    return false;
                }
                try {
                    out_stream.close();
                } catch (Exception e){
                    // nothing to do
                }
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean jpegresult) {
            super.onPostExecute(jpegresult);
            if (jpegresult){
                ByteArrayInputStream byis;
                try {
                     byis = new ByteArrayInputStream(out_stream.toByteArray());
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    // do this only if no changes on image occurred since launch, otherwise ignore result
                    if (imageLastModified<launchTime){
                        Bitmap bitmap_immutable = BitmapFactory.decodeStream(byis);
                        bitmap_visible = bitmap_immutable.copy(Bitmap.Config.ARGB_8888,true);
                        image_display.setImageBitmap(bitmap_visible);
                    }
                } catch (OutOfMemoryError e){
                    jpeg_ecoded_filesize = 0;
                    return;
                }
                try {
                    byis.close();
                } catch (Exception e){
                    // nothing to do
                }
                jpeg_ecoded_filesize = out_stream.size();
                drawCutFrame(cut_line_left,cut_line_top,cut_line_right,cut_line_bottom,scale_ratio);
                next_quality_update_time = new Date().getTime() + NEXT_UPDATE_TIME_DELAY;
                updateImagePropertiesText();
            }
            quality_update_is_dispatched = false;
        }
    }

    private void displayImageQuality(){
        if (!no_image_loaded) {
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            if (new Date().getTime() >= next_quality_update_time) {
                new processJpegInBackground().execute(seekbar_quality.getProgress());
            } else {
                if (!quality_update_is_dispatched) {
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            displayImageQuality();
                        }
                    }, next_quality_update_time - new Date().getTime());
                    quality_update_is_dispatched = true;
                }
            }
        }
    }

    private String generateFileNameSkeleton(int i){
        String skeleton = pref.filename;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
       if (pref.numbering.equals("2")){
            return String.valueOf(i) + "_" + skeleton;
        }
        if (pref.numbering.equals("3")){
            return date.format(calendar.getTime()) + "_" + skeleton + "_" + String.valueOf(i);
        }
        if (pref.numbering.equals("4")){
            return String.valueOf(i) + "_" + skeleton + "_" + date.format(calendar.getTime());
        }
        return skeleton + "_" + String.valueOf(i);
    }

    private String determineUnusedFilename(File path){
        int i = 0;
        String fn;
        File check_file;
        do {
            fn = generateFileNameSkeleton(i) + "."+ImagepipePreferences.getCompressFormatFileExtension(getApplicationContext());
            i = i + 1;
            check_file = new File (path,fn);
        } while (check_file.exists());
        return fn;
    }

    private boolean hasReadWriteStorageRuntimePermission(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_CALLBACK);
                // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int permRequestCode, String perms[], int[] grantRes){
        if (permRequestCode == PERMISSION_CALLBACK){
            int i;
            for (i=0; i<grantRes.length; i++){
                if (perms[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasReadStoragePermission=true;
                    } else {
                        hasReadStoragePermission=false;
                    }
                }
                if (perms[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    if (grantRes[i]==PackageManager.PERMISSION_GRANTED){
                        hasWriteStoragePermission=true;
                    } else {
                        hasWriteStoragePermission=false;
                    }
                }
            }
            if (hasReadStoragePermission && hasWriteStoragePermission) {
                if (missing_permissions_task == MISSING_PERMISSIONS_TASK_sendImageUriIntent) {
                    missing_permissions_task = 0;
                    sendImageUriIntent();
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_pipeTheImage){
                    missing_permissions_task = 0;
                    pipeTheImage(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_bulkpipe){
                    missing_permissions_task = 0;
                    pipeMultipleImages(call_intent);
                }
                if (missing_permissions_task ==  MISSING_PERMISSIONS_TASK_saveImage){
                    missing_permissions_task = 0;
                    saveAndScanImage(true);
                }
            } else {
                if (this.shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        this.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showPermissionsRationale();
                }
            }

        }
    }

    private void showPermissionsRationale(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.dialogpermrationale);
        builder.setTitle(R.string.permissions_title);
        builder.setCancelable(true);
        builder.setNeutralButton(R.string.permissions_ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void sendImageUriIntent() {
        if (!hasReadWriteStorageRuntimePermission()) {
            missing_permissions_task = MISSING_PERMISSIONS_TASK_sendImageUriIntent;
            // break to exit sending here; it will be resumed through the permission callback if necessary
            return;
        }
        if (image_needs_to_be_saved) {
            ImageContainer imageContainer = saveImage(false);
            if (imageContainer != null){
                if (imageContainer.file != null){
                    new FileMediaScanner(getApplicationContext(),imageContainer.file,true);
                }
            }
        } else {
            if (lastImageContainer != null) {
                if (lastImageContainer.uri != null) {
                    shareImage(lastImageContainer);
                }
            }
        }
    }

    private ImageContainer saveImage(Boolean display_save_result){
        ImageContainer imageContainer = new ImageContainer();
        if (no_image_loaded){
            Toast.makeText(this, R.string.toast_load_image_first, Toast.LENGTH_LONG).show();
            return null;
        }
        if (image_needs_to_be_saved) {
            File system_picure_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            String target_directory_string = system_picure_directory.getAbsolutePath()+File.separatorChar+getResources().getString(R.string.app_folder);
            File target_directory = new File(target_directory_string);
            target_directory.mkdirs();
            String targetname = determineUnusedFilename(target_directory);
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            int compression_rate = seekbar_quality.getProgress();
            try {
                imageContainer.file = new File(target_directory, targetname);
                FileOutputStream fos = new FileOutputStream(imageContainer.file);
                Bitmap bitmap_output = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
                bitmap_output.compress(Bitmap.CompressFormat.JPEG, compression_rate, fos);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                Toast.makeText(this, R.string.toast_error_save_failed, Toast.LENGTH_LONG).show();
                return null;
            }
            image_needs_to_be_saved = false;
            if (display_save_result){
            Toast.makeText(this, getResources().getString(R.string.toast_save_successful),Toast.LENGTH_LONG).show();
        }
    } else {
            if (display_save_result) {
                Toast.makeText(this, getResources().getString(R.string.toast_save_not_necessary), Toast.LENGTH_LONG).show();
            }
            return lastImageContainer;
    }
    return imageContainer;
    }

    private void saveAndScanImage(Boolean display_save_result){
        ImageContainer imageContainer = saveImage(display_save_result);
        if (imageContainer != null){
            if (imageContainer.file != null){
                // scan image, update lastImageContainer and do not launch share
                FileMediaScanner fms = new FileMediaScanner(getApplicationContext(),imageContainer.file,false);
            }
        }
    }

    private void shareImage(ImageContainer imageContainer){
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.setDataAndType(imageContainer.uri, "image/jpeg");
        i.putExtra(Intent.EXTRA_STREAM, imageContainer.uri);
        i.setType("image/jpeg");
        if (i.resolveActivity(getPackageManager()) != null){
            try {
                startActivity(Intent.createChooser(i, getResources().getString(R.string.send_image)));
            } catch (ActivityNotFoundException e){
                displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
            }
        } else {
            displayNoActivityFoundToast(ERROR_NO_IMAGE_APP);
        }
    }

    private class FileMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient{

        private File file;
        private Boolean share;
        public MediaScannerConnection mediaScannerConnection;

        public FileMediaScanner(Context c, File f, Boolean s){
            share = s;
            file = f;
            mediaScannerConnection = new MediaScannerConnection(context,this);
            mediaScannerConnection.connect();
        }

        @Override
        public void onMediaScannerConnected() {
           mediaScannerConnection.scanFile(file.getAbsolutePath(),null);
        }

        @Override
        public void onScanCompleted(String s, Uri uri) {
            mediaScannerConnection.disconnect();
            // lastImageContainer needs to be updated with the content-uri. Otherwise, sharing will fail on
            // API > 24.
            lastImageContainer = new ImageContainer();
            lastImageContainer.uri = uri;
            lastImageContainer.file = new File(s);
            if (share) {
                ImageContainer imageContainer = new ImageContainer();
                imageContainer.uri = uri;
                shareImage(imageContainer);
            }
        }
    }

    private String getKilobyteString(long l){
        l = l / 1000;
        return String.valueOf(l)+" kB";
    }

    private void updateImagePropertiesText(){
        String message = "";
        final TextView infotext = (TextView) findViewById(R.id.imageproperties_text);
        if (!no_image_loaded){
            SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
            String filesize ="";
            if ((pref.previewquality) && (jpeg_ecoded_filesize != 0)){
                filesize = getResources().getString(R.string.size)+" "+getKilobyteString(jpeg_ecoded_filesize);
                if (original_image_filesize != 0){
                    filesize =  filesize + " ("+String.valueOf((jpeg_ecoded_filesize *100)/ original_image_filesize)+"%)";
                }
            } else filesize = "";
            message = getResources().getString(R.string.width)+" "+String.valueOf(bitmap_image.getWidth())+" "+getResources().getString(R.string.height)+" "+String.valueOf(bitmap_image.getHeight())+" "+getResources().getString(R.string.quality)+" "+String.valueOf(seekbar_quality.getProgress()+" "+filesize+debug);
        } else {
            // keep message empty, nothing to do
        }
        final String s = message;
                runOnUiThread(new Runnable() {
            @Override
            public void run() {
                infotext.setText(s);
            }
        });
    }

    private void initOriginalBitmap(){
        if (bitmap_image!=null){
            try {
                bitmap_original = bitmap_image.copy(Bitmap.Config.ARGB_8888,true);
            } catch (Exception e){
                // out of memory; keep bitmap_original empty
                bitmap_original = null;
            }
        }
    }

    private void setEmptyDefaultScreen() {
        // clear cache because there is no image.
        clearCache();
        // View main_layout = (View) findViewById(R.layout.activity_image_receiver);
        View main_view = getWindow().getDecorView().getRootView();
        main_view.post(new Runnable(){
            @Override
            public void run(){
                Bitmap add_icon_blue = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_choose_blue,null);
                int x = image_display.getWidth();
                int y = image_display.getHeight();
                if ((x!=0) && (y!=0)){
                    Bitmap original = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
                    Canvas no_image_canvas = new Canvas();
                    no_image_canvas.setBitmap(original);
                    x1_choose_button = (x-add_icon_blue.getWidth())/2;
                    y1_choose_button = (y-add_icon_blue.getHeight())/2;
                    x2_choose_button = (x+add_icon_blue.getWidth())/2;
                    y2_choose_button = (y+add_icon_blue.getHeight())/2;
                    no_image_canvas.drawBitmap(add_icon_blue,x1_choose_button,y1_choose_button,null);
                    Paint paint = new Paint();
                    paint.setColor(getAppColor(R.color.primaryTextColor));
                    int text_size = y / 10;
                    paint.setTextSize(text_size);
                    float text_width = paint.measureText(getResources().getString(R.string.empty_imageview_text));
                    while (paint.measureText(getResources().getString(R.string.empty_imageview_text))>x-x/20) {
                        text_size = text_size - 1;
                        paint.setTextSize(text_size);
                    }
                    no_image_canvas.drawText(getResources().getString(R.string.empty_imageview_text), x/40, (y-add_icon_blue.getHeight())/2 - text_size*2,paint);
                    if (original != null) {
                        bitmap_image = original.copy(Bitmap.Config.ARGB_8888,true);
                        updateImageDisplay();
                        no_image_loaded = true;
                        resetCutFrame();
                    }
                    showNewCanvasButton();
                } else {
                    // do nothing
                }
            }});
    }

    private void resetCutFrame(){
        cut_line_bottom = bitmap_image.getHeight();
        cut_line_left = 0;
        cut_line_right = bitmap_image.getWidth();
        cut_line_top = 0;
        getScaleRatio();
    }

    private void pickImageFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent,SELECT_FROM_GALLERY_CALLBACK);
    }

    private Boolean readImageBitmapFromUri(Uri source_uri){
        try {
            if (pref == null){
                readPreferences();
            }
            // free memory
            bitmap_original = null;
            InputStream image_inputstream = new BufferedInputStream(this.getContentResolver().openInputStream(source_uri));
            ByteArrayOutputStream image_bytestream = new ByteArrayOutputStream();
            // if autorotate is enabled, file is temporarily saved in cache for further use to determine rotation later.
            File tempfile = new File(getApplicationContext().getCacheDir(),TEMP_FILENAME);
            FileOutputStream fileOutputStream = new FileOutputStream(tempfile);
            byte [] bytebuffer = new byte[1024*16];
            int i = 0;
            while ((i = image_inputstream.read(bytebuffer)) != -1){
                image_bytestream.write(bytebuffer,0,i);
                if (pref.autorotate){
                    // only write to cache if autorotate is enabled.
                    fileOutputStream.write(bytebuffer);
                }
            }
            image_inputstream.close();
            fileOutputStream.close();
            original_image_filesize = image_bytestream.size();
            // determine samṕleSize
            BitmapFactory.Options options = new BitmapFactory.Options();
            int neededSampleSize = 1;
            if ((pref.autoscale) || (pref.forcedownscale)){
                int maxsize = getScreenDownscaleValue();
                if (pref.autoscale){
                    int autoscaleValue = Integer.parseInt(pref.scalemax);
                    if (autoscaleValue<maxsize){
                        maxsize = autoscaleValue;
                    }
                }
                ByteArrayInputStream bis = new ByteArrayInputStream(image_bytestream.toByteArray());
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(bis,null,options);
                bis.close();
                if ((options.outHeight>maxsize) || (options.outWidth>maxsize)){
                    int ySampleSize=Math.round((float)options.outHeight/(float)maxsize);
                    int xSampleSize=Math.round((float)options.outWidth/(float)maxsize);
                    neededSampleSize = Math.min(ySampleSize, xSampleSize);
                }
                options = new BitmapFactory.Options();
                options.inSampleSize = neededSampleSize;
            }
            options.inMutable=true;
            ByteArrayInputStream bis = new ByteArrayInputStream(image_bytestream.toByteArray());
            bitmap_image = BitmapFactory.decodeStream(bis,null,options);
            bis.close();
            image_bytestream.close();
            // save original source & filesize
            ImagepipePreferences.setImageContainer(context,source_uri,original_image_filesize);
            originalImageContainer = new ImageContainer(source_uri,original_image_filesize);
        }
        catch (Exception e){
            return false;
        }
        no_image_loaded = false;
        image_needs_to_be_saved = true;
        return true;
    }

    public void showLoadingFailedErrorToast(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, R.string.toast_error_image_not_loaded, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int rc, int rescode, Intent data){
        if ((rc==SELECT_FROM_GALLERY_CALLBACK) && (rescode==RESULT_OK)){
            call_intent = data;
            Uri i_uri = data.getData();
            if (i_uri != null) {
                image_display.setImageBitmap(null);
                clearCache();
                if (!readImageBitmapFromUri(i_uri)){
                    showLoadingFailedErrorToast();
                }
                gallery_already_called = false;
                applyChangesOnLoadedImage(i_uri);
                lastImageContainer = new ImageContainer();
                lastImageContainer.uri = i_uri;
                initOriginalBitmap();
            }
        } else {
            // this means the gallery did not return us an image
            gallery_already_called = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle save_bundle){
        super.onSaveInstanceState(save_bundle);
        save_bundle.putFloat(SIS_CUT_LINE_TOP,cut_line_top);
        save_bundle.putFloat(SIS_CUT_LINE_BOTTOM,cut_line_bottom);
        save_bundle.putFloat(SIS_CUT_LINE_LEFT,cut_line_left);
        save_bundle.putFloat(SIS_CUT_LINE_RIGHT,cut_line_right);
        save_bundle.putFloat(SIS_SCALE_RATIO,scale_ratio);
        save_bundle.putBoolean(SIS_IMAGE_NEEDS_TO_BE_SAVED,image_needs_to_be_saved);
        if (lastImageContainer != null){
            if (lastImageContainer.uri!=null) {
                save_bundle.putParcelable(SIS_LAST_IMAGE_URI,lastImageContainer.uri);
            }
        }
        save_bundle.putBoolean(SIS_NO_IMAGE_LOADED,no_image_loaded);
        save_bundle.putLong(SIS_JPEG_FILESIZE,jpeg_ecoded_filesize);
        save_bundle.putLong(SIS_ORIGINAL_FILESIZE,original_image_filesize);
        save_bundle.putBoolean(SIS_PIPING_ALREADY_LAUNCHED,piping_was_already_launched);
        save_bundle.putInt(SIS_CACHE_POSITION,cachePosition);
        save_bundle.putInt(SIS_PAINTTOOL,paintTool);
        save_bundle.putInt(SIS_BRUSHSIZE,brushSize);
        save_bundle.putInt(SIS_SELECTEDCOLOR,selectedColor);
        save_bundle.putCharSequence(SIS_CHARSEQUENCE,charSequence);
        save_bundle.putBoolean(SIS_TEXTSTYLE_BOLD,textStyle_bold);
        save_bundle.putBoolean(SIS_TEXTSTYLE_ITALIC,textStyle_italic);
        save_bundle.putBoolean(SIS_TEXTSTYLE_UNDERLINE,textStyle_underline);
        save_bundle.putBoolean(SIS_TEXTSTYLE_STRIKETHROUGH,textStyle_strikethrough);
    }

    private void getSavedInstanceStateCutLine(Bundle bundle){
        if (bitmap_image!=null){
            cut_line_top=bundle.getFloat(SIS_CUT_LINE_TOP,0);
            cut_line_bottom=bundle.getFloat(SIS_CUT_LINE_BOTTOM, bitmap_image.getHeight());
            cut_line_left=bundle.getFloat(SIS_CUT_LINE_LEFT,0);
            cut_line_right=bundle.getFloat(SIS_CUT_LINE_RIGHT,bitmap_image.getWidth());
        }
        scale_ratio=bundle.getFloat(SIS_SCALE_RATIO,getScaleRatio());
    }

    private void readPreferences(){
        pref = new ImagepipePreferences(this);
        pref.readPreferences();
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        scale_view.setText(pref.scalemax);
        seekbar_quality.setMax(pref.getQualitymaxvalue());
        seekbar_quality.setProgress(pref.quality);
        original_image_filesize = pref.ic_filesize;
        originalImageContainer = ImagepipePreferences.getImageContainer(context);
    }

    private void writePreferences(){
        TextView scale_view = (TextView) findViewById(R.id.editText_size);
        SeekBar seekbar_quality = (SeekBar) findViewById(R.id.seekBar_quality);
        pref.scalemax = scale_view.getText().toString();
        pref.quality = seekbar_quality.getProgress();
        pref.savePreferences();
    }

    public void showIntroDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View aboutDialogView = getLayoutInflater().inflate(R.layout.aboutdialog,null);
        builder.setView(aboutDialogView);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setCancelable(true);
        builder.setIcon(R.drawable.appicontoplayer);
        builder.setNeutralButton(R.string.intro_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        String versioning = BuildConfig.VERSION_NAME + " (build "+BuildConfig.VERSION_CODE+")";
        TextView heading = (TextView) aboutDialogView.findViewById(R.id.text_intro_headtext);
        heading.setText(getResources().getString(R.string.intro_headtext_text)+System.getProperty("line.separator")+"version "+versioning);
    }

    public void showExifDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.exif_tags));
        builder.setCancelable(true);
        builder.setMessage(getExifInfoText());
        builder.setNeutralButton(R.string.permissions_ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addTagLine(ExifInterface exif_data, StringBuilder stringBuilder, int min_api_level, String label, String tag){
        try{
            if (android.os.Build.VERSION.SDK_INT >= min_api_level) {
                String attribute = exif_data.getAttribute(tag);
                if (attribute != null){
                    stringBuilder.append(label + ": "+ attribute + System.lineSeparator());
                }
            }
        } catch (Exception e){
            // unknown tag in api, ignore
        }
    }

    @SuppressWarnings("deprecation")
    private String getExifInfoText() {
        ExifInterface exif_data = null;
        if (originalImageContainer.uri!=null){
            try {
                InputStream image_inputstream = new BufferedInputStream(this.getContentResolver().openInputStream(originalImageContainer.uri));
                exif_data = new ExifInterface(image_inputstream);
            } catch (Exception e){
                return "";
            }
        }
        if (exif_data==null){
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (exif_data != null){
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_aperture),ExifInterface.TAG_APERTURE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_aperture),ExifInterface.TAG_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_maxapterture),ExifInterface.TAG_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_artist),ExifInterface.TAG_ARTIST);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_bps),ExifInterface.TAG_BITS_PER_SAMPLE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_brightness),ExifInterface.TAG_BRIGHTNESS_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cfa),ExifInterface.TAG_CFA_PATTERN);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cspace),ExifInterface.TAG_COLOR_SPACE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_compcfg),ExifInterface.TAG_COMPONENTS_CONFIGURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cbpp),ExifInterface.TAG_COMPRESSED_BITS_PER_PIXEL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_compression),ExifInterface.TAG_COMPRESSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_contrast),ExifInterface.TAG_CONTRAST);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_copyright),ExifInterface.TAG_COPYRIGHT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_cr),ExifInterface.TAG_CUSTOM_RENDERED);
            addTagLine(exif_data,stringBuilder,14,getApplicationContext().getResources().getString(R.string.exiflabel_datetime),ExifInterface.TAG_DATETIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_datetimed),ExifInterface.TAG_DATETIME_DIGITIZED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_datetimeo),ExifInterface.TAG_DATETIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_dcs),ExifInterface.TAG_DEFAULT_CROP_SIZE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ds),ExifInterface.TAG_DEVICE_SETTING_DESCRIPTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_digzoom),ExifInterface.TAG_DIGITAL_ZOOM_RATIO);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_dng),ExifInterface.TAG_DNG_VERSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_exifv),ExifInterface.TAG_EXIF_VERSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ebv),ExifInterface.TAG_EXPOSURE_BIAS_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expi),ExifInterface.TAG_EXPOSURE_INDEX);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expm),ExifInterface.TAG_EXPOSURE_MODE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_expp),ExifInterface.TAG_EXPOSURE_PROGRAM);
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_expt),ExifInterface.TAG_EXPOSURE_TIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fs),ExifInterface.TAG_FILE_SOURCE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_flash),ExifInterface.TAG_FLASH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_flashe),ExifInterface.TAG_FLASH_ENERGY);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpv),ExifInterface.TAG_FLASHPIX_VERSION);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_focallength),ExifInterface.TAG_FOCAL_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_focallength35),ExifInterface.TAG_FOCAL_LENGTH_IN_35MM_FILM);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpru),ExifInterface.TAG_FOCAL_PLANE_RESOLUTION_UNIT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpx),ExifInterface.TAG_FOCAL_PLANE_X_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fpy),ExifInterface.TAG_FOCAL_PLANE_Y_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_fnumber),ExifInterface.TAG_F_NUMBER);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gaincontrol),ExifInterface.TAG_GAIN_CONTROL);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_altitude),ExifInterface.TAG_GPS_ALTITUDE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_gps_longitude),ExifInterface.TAG_GPS_LONGITUDE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_gps_latitude),ExifInterface.TAG_GPS_LATITUDE);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_latitude_r),ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
            addTagLine(exif_data,stringBuilder,9,getApplicationContext().getResources().getString(R.string.exiflabel_gps_altitude_r),ExifInterface.TAG_GPS_ALTITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_areainfo),ExifInterface.TAG_GPS_AREA_INFORMATION);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_datestamp),ExifInterface.TAG_GPS_DATESTAMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_bearing),ExifInterface.TAG_GPS_DEST_BEARING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_bearingref),ExifInterface.TAG_GPS_DEST_BEARING_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_distance),ExifInterface.TAG_GPS_DEST_DISTANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_distanceref),ExifInterface.TAG_GPS_DEST_DISTANCE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlat),ExifInterface.TAG_GPS_DEST_LATITUDE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlatref),ExifInterface.TAG_GPS_DEST_LATITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlong),ExifInterface.TAG_GPS_DEST_LONGITUDE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_destlongref),ExifInterface.TAG_GPS_DEST_LONGITUDE_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_differential),ExifInterface.TAG_GPS_DIFFERENTIAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_dop),ExifInterface.TAG_GPS_DOP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_img),ExifInterface.TAG_GPS_IMG_DIRECTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_imgdir),ExifInterface.TAG_GPS_IMG_DIRECTION_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_mapdatum),ExifInterface.TAG_GPS_MAP_DATUM);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_measuremode),ExifInterface.TAG_GPS_MEASURE_MODE);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_processingmethod),ExifInterface.TAG_GPS_PROCESSING_METHOD);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_satellites),ExifInterface.TAG_GPS_SATELLITES);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_speed),ExifInterface.TAG_GPS_SPEED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_speedref),ExifInterface.TAG_GPS_SPEED_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_status),ExifInterface.TAG_GPS_STATUS);
            addTagLine(exif_data,stringBuilder,8,getApplicationContext().getResources().getString(R.string.exiflabel_gps_timestamp),ExifInterface.TAG_GPS_TIMESTAMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_track),ExifInterface.TAG_GPS_TRACK);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_trackref),ExifInterface.TAG_GPS_TRACK_REF);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_gps_versionid),ExifInterface.TAG_GPS_VERSION_ID);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_image_description),ExifInterface.TAG_IMAGE_DESCRIPTION);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_image_length),ExifInterface.TAG_IMAGE_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_image_unique_id),ExifInterface.TAG_IMAGE_UNIQUE_ID);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_image_width),ExifInterface.TAG_IMAGE_WIDTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_interop_index),ExifInterface.TAG_INTEROPERABILITY_INDEX);
            addTagLine(exif_data,stringBuilder,11,getApplicationContext().getResources().getString(R.string.exiflabel_iso),ExifInterface.TAG_ISO);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_iso),ExifInterface.TAG_ISO_SPEED_RATINGS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_jpegformat),ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_jpegformat_length),ExifInterface.TAG_JPEG_INTERCHANGE_FORMAT_LENGTH);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ls),ExifInterface.TAG_LIGHT_SOURCE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_make),ExifInterface.TAG_MAKE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_makernote),ExifInterface.TAG_MAKER_NOTE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_max_aperture_value),ExifInterface.TAG_MAX_APERTURE_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_metering_mode),ExifInterface.TAG_METERING_MODE);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_model),ExifInterface.TAG_MODEL);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_new_subfile_type),ExifInterface.TAG_NEW_SUBFILE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_oecf),ExifInterface.TAG_OECF);
            // api 30 not implemented yet
            //addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time),ExifInterface.TAG_OFFSET_TIME);
            //addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time_digitized),ExifInterface.TAG_OFFSET_TIME_DIGITIZED);
            // addTagLine(exif_data,stringBuilder,30,getApplicationContext().getResources().getString(R.string.exiflabel_offset_time_original),ExifInterface.TAG_OFFSET_TIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_orientation),ExifInterface.TAG_ORIENTATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pi),ExifInterface.TAG_PHOTOMETRIC_INTERPRETATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pixel_x_dim),ExifInterface.TAG_PIXEL_X_DIMENSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_pixel_y_dim),ExifInterface.TAG_PIXEL_Y_DIMENSION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_planarconf),ExifInterface.TAG_PLANAR_CONFIGURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_prim_chromat),ExifInterface.TAG_PRIMARY_CHROMATICITIES);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_bw),ExifInterface.TAG_REFERENCE_BLACK_WHITE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_related_soundfile),ExifInterface.TAG_RELATED_SOUND_FILE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_resolution_unit),ExifInterface.TAG_RESOLUTION_UNIT);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_rps),ExifInterface.TAG_ROWS_PER_STRIP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_samples_per_pixel),ExifInterface.TAG_SAMPLES_PER_PIXEL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_saturation),ExifInterface.TAG_SATURATION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_scene_capture_type),ExifInterface.TAG_SCENE_CAPTURE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_scene_type),ExifInterface.TAG_SCENE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_sensing_method),ExifInterface.TAG_SENSING_METHOD);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_sharpness),ExifInterface.TAG_SHARPNESS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_shutterspeedvalue),ExifInterface.TAG_SHUTTER_SPEED_VALUE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_software),ExifInterface.TAG_SOFTWARE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_spatial_frequency_response),ExifInterface.TAG_SPATIAL_FREQUENCY_RESPONSE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_spectral_sensitivity),ExifInterface.TAG_SPECTRAL_SENSITIVITY);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_strip_byte_counts),ExifInterface.TAG_STRIP_BYTE_COUNTS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_strip_offsets),ExifInterface.TAG_STRIP_OFFSETS);
            addTagLine(exif_data,stringBuilder,26,getApplicationContext().getResources().getString(R.string.exiflabel_subfile_type),ExifInterface.TAG_SUBFILE_TYPE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_area),ExifInterface.TAG_SUBJECT_AREA);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_dist),ExifInterface.TAG_SUBJECT_DISTANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_distance_range),ExifInterface.TAG_SUBJECT_DISTANCE_RANGE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subject_loc),ExifInterface.TAG_SUBJECT_LOCATION);
            addTagLine(exif_data,stringBuilder,23,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time),ExifInterface.TAG_SUBSEC_TIME);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time_dig),ExifInterface.TAG_SUBSEC_TIME_DIGITIZED);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_subsec_time_orig),ExifInterface.TAG_SUBSEC_TIME_ORIGINAL);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_transfer),ExifInterface.TAG_TRANSFER_FUNCTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_user_comment),ExifInterface.TAG_USER_COMMENT);
            addTagLine(exif_data,stringBuilder,5,getApplicationContext().getResources().getString(R.string.exiflabel_white_balance),ExifInterface.TAG_WHITE_BALANCE);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_white_point),ExifInterface.TAG_WHITE_POINT);
            // addTagLine(exif_data,stringBuilder,29,getApplicationContext().getResources().getString(R.string.exiflabel_xmp),ExifInterface.TAG_XMP);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_x_resolution),ExifInterface.TAG_X_RESOLUTION);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_co),ExifInterface.TAG_Y_CB_CR_COEFFICIENTS);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_pos),ExifInterface.TAG_Y_CB_CR_POSITIONING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_ycbcr_sub),ExifInterface.TAG_Y_CB_CR_SUB_SAMPLING);
            addTagLine(exif_data,stringBuilder,24,getApplicationContext().getResources().getString(R.string.exiflabel_y_resolution),ExifInterface.TAG_Y_RESOLUTION);
        }
        return stringBuilder.toString();
    }

    private static boolean textToolVisible = false;
    private static boolean brushToolVisible = false;
    private static boolean paletteToolVisible = false;
    private static boolean hintBubbleVisible = false;

    private void hideBrushSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout brush_selector = (RelativeLayout) findViewById(R.id.brushview_maincontainer);
        main_layout.removeView(brush_selector);
        brushToolVisible = false;
    }

    private void setPreviewBrushBitmapToColor(ImageView imageView){
        Bitmap bitmapIcon_immutable = BitmapFactory.decodeResource(getResources(),R.drawable.symbol_circle);
        Bitmap bitmapIcon = bitmapIcon_immutable.copy(Bitmap.Config.ARGB_8888,true);
        for (int y=0; y<bitmapIcon.getHeight(); y++){
            for (int x=0; x<bitmapIcon.getWidth(); x++){
                if (bitmapIcon.getPixel(x,y)!=Color.TRANSPARENT){
                    bitmapIcon.setPixel(x,y,selectedColor);
                }
            }
        }
        imageView.setImageBitmap(bitmapIcon);
    }

    private void previewBrushSize(final ImageView imageView){
        // get metrics of main image view
        ViewGroup.LayoutParams lp = imageView.getLayoutParams();
        float scale = getScaleRatio();
        lp.width = Math.round(brushSize/scale);
        lp.height =Math.round(brushSize/scale);
        imageView.setLayoutParams(lp);
    }

    public void showBrushSelector(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View selectorView = getLayoutInflater().inflate(R.layout.view_brushsize,main_layout,true);
        Button button = (Button) selectorView.findViewById(R.id.brushview_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideBrushSelector();
            }
        });
        final ImageView brushPreview = (ImageView) selectorView.findViewById(R.id.brushview_sizepreview);
        setPreviewBrushBitmapToColor(brushPreview);
        previewBrushSize(brushPreview);
        SeekBar seekBar = (SeekBar) selectorView.findViewById(R.id.seekBar_brushsize);
        seekBar.setProgress(brushSize);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                brushSize = seekBar.getProgress();
                previewBrushSize(brushPreview);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // do nothing
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                hideBrushSelector();
            }
        });
        brushToolVisible = true;
    }

    public void showTextSelector(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View selectorView = getLayoutInflater().inflate(R.layout.view_charsequence,main_layout,true);
        Button button = selectorView.findViewById(R.id.charsequenceview_button);
        EditText editText = selectorView.findViewById(R.id.charsequenceview_text);
        CheckBox checkBold = (CheckBox) findViewById(R.id.charsequenceview_ckeck_bold);
        CheckBox checkItalic = (CheckBox) findViewById(R.id.charsequenceview_ckeck_italic);
        CheckBox checkUnderline = (CheckBox) findViewById(R.id.charsequenceview_ckeck_underline);
        CheckBox checkStrikethrough = (CheckBox) findViewById(R.id.charsequenceview_ckeck_strikethrough);
        if (!charSequence.equals("")){
            editText.setText(charSequence);
        }
        checkBold.setChecked(textStyle_bold);
        checkItalic.setChecked(textStyle_italic);
        checkUnderline.setChecked(textStyle_underline);
        checkStrikethrough.setChecked(textStyle_strikethrough);
        checkBold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_bold = b;
            }
        });
        checkItalic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_italic = b;
            }
        });
        checkUnderline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_underline = b;
            }
        });
        checkStrikethrough.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                textStyle_strikethrough = b;
            }
        });
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideTextSelector();
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }
            @Override
            public void afterTextChanged(Editable editable) {
                charSequence = editable;
            }
        });
        textToolVisible = true;
    }

    private void hideTextSelector(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout text_selector = (RelativeLayout) findViewById(R.id.charsequenceview_maincontainer);
        EditText editText = (EditText) findViewById(R.id.charsequenceview_text);
        if (editText!=null){
            charSequence = editText.getText();
        }
        // close soft keyboard
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager!=null){
            View target_view = (View) findViewById(R.id.charsequenceview_text);
            if (target_view==null){
                target_view = new View(this);
            }
            inputMethodManager.hideSoftInputFromWindow(target_view.getWindowToken(),0);
        }
        main_layout.removeView(text_selector);
        textToolVisible = false;
    }

    static int[] COLORPALETTE = {0xff000000,0xff0000AB,0xff00AB00,0xff00ABAB,0xffAB0000,0xffAB00AB,0xffAB5600,0xffBBBBBB,
                                 0xff666666,0xff6666FF,0xff66FF66,0xff66FFFF,0xffFF6666,0xffFF66FF,0xffFFFF55,0xffFFFFFF,
                                 0xfffcf309,0xffd2cc1b,0xffe2ad20,0xfff6c266,0xff896c1e,0xffed7a14,0xffaa6121,0xff57381c};

    public void showPalette(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View paletteView = getLayoutInflater().inflate(R.layout.view_colorpicker,main_layout,true);
        for (int i=0; i<COLORPALETTE.length; i++){
            int id = getResources().getIdentifier("brushview_button"+i,"id",this.getPackageName());
            ImageButton imageButton = (ImageButton) findViewById(id);
            imageButton.setOnClickListener(colorButtonListener);
        }
        paletteToolVisible = true;
    }

    public void hidePalette(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout palette_view = (RelativeLayout) findViewById(R.id.colorpicker_maincontainer);
        main_layout.removeView(palette_view);
        paletteToolVisible = false;
    }

    public void colorButtonPressed(View view){
        String name = getResources().getResourceName(view.getId());
        int charPos = name.indexOf("button");
        String numberString = name.substring(charPos+6);
        int position = Integer.parseInt(numberString);
        selectedColor = COLORPALETTE[position];
        buttonColor.setColorFilter(selectedColor,PorterDuff.Mode.SRC_ATOP);
        hidePalette();
    }

    private String getHintText(){
        if (pref.bubblecounter==0){
            return getResources().getString(R.string.hint1_line1) + System.getProperty("line.separator") + System.getProperty("line.separator") + getResources().getString(R.string.hint1);
        } else {
            return getResources().getString(R.string.hint2);
        }
    }


    public void showHintBubble(){
        // close tool views if visible to avoid overlapping
        hideToolsIfVisible();
        // add view
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        View paletteView = getLayoutInflater().inflate(R.layout.view_hint,main_layout,true);
        TextView text_hint = (TextView) findViewById(R.id.hintview_text);
        text_hint.setText(getHintText());
        Button button_hint = (Button) findViewById(R.id.hintview_button);
        button_hint.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHintBubble();
            }
        });
        CheckBox checkBox = (CheckBox) findViewById(R.id.hintview_checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ImagepipePreferences.setShouldShowHints(context,!b);
            }
        });
        hintBubbleVisible = true;
        pref.bubblecounter = pref.bubblecounter + 1;
        if (pref.bubblecounter>HINT_COUNT){
            pref.bubblecounter = 0;
        }
        pref.savePreferences();
    }

    public void hideHintBubble(){
        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.popup_hook);
        RelativeLayout hint_view = (RelativeLayout) findViewById(R.id.hintview_maincontainer);
        main_layout.removeView(hint_view);
        hintBubbleVisible = false;
    }

    public void displayHintBubble(){
        Handler bubble_handler = new Handler();
        bubble_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showHintBubble();
            }
        }, BUBBLE_RUN_DELAY);
    }

    private void checkForHintBubbleDisplay(){
        if ((no_image_loaded) && (ImagepipePreferences.shouldShowHints(context))) {
            displayHintBubble();
        }
    }

    public void hideToolsIfVisible(){
        if (textToolVisible){
            hideTextSelector();
        }
        if (brushToolVisible){
            hideBrushSelector();
        }
        if (paletteToolVisible){
            hidePalette();
        }
        if (hintBubbleVisible){
            hideHintBubble();
        }
    }

    public boolean readImageBitmapFromCache(int position){
        Bitmap bitmap = PictureCache.restoreFromPictureCache(context,position);
        if (bitmap!=null){
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            updateImageDisplay();
            return true;
        }
        if (bitmap_original==null){
            bitmap_original = PictureCache.restoreFromPictureCache(context,0);
        }
        return false;
    }

    public void readLastImageFromCacheAndRestoreCache(){
        // empty cache returns cachePosition = 0,
        // all bitmaps may be null if cache is empty
        cachePosition = PictureCache.getLastUsedCachePosition(context);
        bitmap_original = PictureCache.restoreFromPictureCache(context,0);
        bitmap_image = PictureCache.restoreFromPictureCache(context,cachePosition);
        bitmap_visible = bitmap_image;
        if ((bitmap_image==null) && (bitmap_original!=null)){
            bitmap_image = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap_original.copy(Bitmap.Config.ARGB_8888,true);
        }
        if (bitmap_image==null){
            no_image_loaded = true;
        } else {
            no_image_loaded = false;
            image_needs_to_be_saved = true;
            lastImageContainer = null;
        }
    }

    public void putImageBitmapToCache(){
        if (bitmap_image!=null){
            PictureCache.clearPictureCache(context,cachePosition+1);
            cachePosition = PictureCache.saveToPictureCache(context,bitmap_image);
            hideNewCanvasButton();
        }
    }

    public void clearCache(){
        PictureCache.clearPictureCache(context);
        cachePosition = 0;
    }

    public void unDoLastAction(){
        if (cachePosition>0){
            cachePosition--;
            Bitmap bitmap = PictureCache.restoreFromPictureCache(context,cachePosition);
            PictureCache.touchPicture(context,cachePosition);
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            image_display.setImageBitmap(bitmap_visible);
            setLastModifiedTimestamp();
            updateImageDisplay();
        }
    }

    public void reDoAction(){
        if (cachePosition<PictureCache.getPictureCacheSize(context)-1){
            cachePosition++;
            Bitmap bitmap = PictureCache.restoreFromPictureCache(context,cachePosition);
            PictureCache.touchPicture(context,cachePosition);
            bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
            image_display.setImageBitmap(bitmap_visible);
            setLastModifiedTimestamp();
            updateImageDisplay();
        }
    }

    private final static String NEWCANVAS_AUTHORITY="IMAGEPIPE-NEW";

    public void createNewCanvas() {
        lastImageContainer = null;
        image_needs_to_be_saved = true;
        no_image_loaded = false;
        ImagepipePreferences.resetImageContainer(context);
        int size = Integer.parseInt(pref.scalemax);
        Bitmap bitmap = Bitmap.createBitmap(size,size, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(selectedColor);
        bitmap_image = bitmap.copy(Bitmap.Config.ARGB_8888,true);
        bitmap_original = bitmap.copy(Bitmap.Config.ARGB_8888,false);
        bitmap_visible = bitmap.copy(Bitmap.Config.ARGB_8888,true);
        image_display.setImageBitmap(bitmap_visible);
        clearCache();
        putImageBitmapToCache();
        if (newCanvas!=null){
            newCanvas.setVisibility(View.INVISIBLE);
        }
        lastImageContainer = new ImageContainer(Uri.fromParts("file:",NEWCANVAS_AUTHORITY,""),bitmap_original.getByteCount());
        ImagepipePreferences.setImageContainer(context,lastImageContainer);
        originalImageContainer = new ImageContainer(lastImageContainer);
        updateImageDisplay();
    }

    public void showNewCanvasButton(){
        if (newCanvas==null){
            newCanvas = (ImageButton) findViewById(R.id.new_canvas);
        }
        if (newCanvas!=null){
            newCanvas.setVisibility(View.VISIBLE);
            newCanvas.setOnClickListener(newCanvasListener);
        }
    }

    public void hideNewCanvasButton(){
        if (newCanvas==null){
            newCanvas = (ImageButton) findViewById(R.id.new_canvas);
        }
        if (newCanvas!=null){
            if (newCanvas.getVisibility() != View.INVISIBLE){
                newCanvas.setVisibility(View.INVISIBLE);
            }
        }
    }

    final static int FLIP_HORIZONTALLY=0;
    final static int FLIP_VERTICALLY=1;

    public boolean flipImage(int direction){
        if (!no_image_loaded){
            Matrix matrix = new Matrix();
            if (direction==FLIP_HORIZONTALLY){
                matrix.preScale(-1,1);
                float i = cut_line_right; cut_line_right = bitmap_image.getWidth() - cut_line_left; cut_line_left = bitmap_image.getWidth() - i;
            } else {
                float i = cut_line_top; cut_line_top = bitmap_image.getHeight() - cut_line_bottom; cut_line_bottom = bitmap_image.getHeight() - i;
                matrix.preScale(1,-1);
            }
            try {
                // perform operation on a *new* bitmap to avoid nullifying bitmap_image when out of memory
                Bitmap bitmapTemp = Bitmap.createBitmap(bitmap_image,0,0,bitmap_image.getWidth(),bitmap_image.getHeight(),matrix,true);
                if (bitmapTemp!=null){
                    bitmap_image = bitmapTemp.copy(Bitmap.Config.ARGB_8888,true);
                }
                putImageBitmapToCache();
                updateImageDisplay();
                updateImagePropertiesText();
            } catch (OutOfMemoryError e){
                return false;
            }
        }
        return true;
    }

    private void clearImageData(){
        // clear image cache
        clearCache();
        // clear cache from image rotation
        deleteCacheFile();
        no_image_loaded = true;
        bitmap_image = null;
        bitmap_visible = null;
        bitmap_original = null;
        lastImageContainer = null;
        ImagepipePreferences.resetImageContainer(context);
        updateImageDisplay();
        updateImagePropertiesText();
    }

}
