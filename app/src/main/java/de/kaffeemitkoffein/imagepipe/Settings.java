/**
 * This file is part of Imagepipe.
 *
 * Copyright (c) 2017, 2018, 2019, 2020 Pawel Dube
 *
 * Imagepipe is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * Imagepipe is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imagepipe. If not, see <http://www.gnu.org/licenses/>.
 */

package de.kaffeemitkoffein.imagepipe;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    private static final String PREF_FILENAME = "PREF_filename";
    private static final String PREF_NUMBERING = "PREF_numbering";
    private static final String PREF_MAXQUALITY = "PREF_maxquality";

    @Override
    @SuppressWarnings("deprecation")
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        updateValuesDisplay();
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onPause(){
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @SuppressWarnings("deprecation")
    private void updateValuesDisplay(){
        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(this);
        Preference p;
        String file_ext;
        String[] file_ext_text = getResources().getStringArray(R.array.fileextension_text);
        String s=sp.getString(PREF_FILENAME,"");
        p = findPreference(PREF_FILENAME);
        p.setSummary(getResources().getString(R.string.preference_filename_summary)+" "+s);
        s=sp.getString(PREF_NUMBERING,"");
        try {
        file_ext = file_ext_text[Integer.valueOf(s)-1];
        } catch (NumberFormatException e){
            file_ext = "";
        }
        p = findPreference(PREF_NUMBERING);
        p.setSummary(getResources().getString(R.string.preference_numbering_summary)+" "+file_ext);
        p = findPreference(PREF_MAXQUALITY);
        p.setSummary(getResources().getString(R.string.preference_maxquality_summary)+" "+String.valueOf(sp.getString(PREF_MAXQUALITY,"80")));
    }

    public void onSharedPreferenceChanged(SharedPreferences sp, String id){
        updateValuesDisplay();
    }
}